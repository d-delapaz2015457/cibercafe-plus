'------------------------------------------------------------------------------
' <auto-generated>
'     Este código se generó a partir de una plantilla.
'
'     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
'     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System
Imports System.Collections.Generic

Partial Public Class Terminales
    Public Property idTerminal As Integer
    Public Property name As String
    Public Property direcIP As String
    Public Property estado As Nullable(Of Boolean)

    Public Overridable Property Registros As ICollection(Of Registros) = New HashSet(Of Registros)

End Class
