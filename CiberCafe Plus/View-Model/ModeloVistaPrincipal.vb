﻿Imports System.ComponentModel
Imports System.Windows.Threading
Imports System.DateTime

Public Class ModeloVistaPrincipal
    Implements INotifyPropertyChanged, ICommand
    Private _Main As MainWindow

    '-----------------------NUMEROS
    Public contar As Integer = 0
    Public contar2 As Integer = 0
    Public contar3 As Integer = 0
    Public contar4 As Integer = 0
    Public contar5 As Integer = 0
    Public contar6 As Integer = 0
    Public contar7 As Integer = 0
    Public contar8 As Integer = 0
    Public contar9 As Integer = 0
    Public contar10 As Integer = 0
    Public contar11 As Integer = 0
    Public contar12 As Integer = 0
    Public contar13 As Integer = 0
    Public contar14 As Integer = 0
    '------------------------------

    Private _TextBot1 As String = "Sin conexión"
    Private _FondoC1 As New SolidColorBrush(Colors.Transparent)
    Private _FondoD1 As New SolidColorBrush(Colors.Transparent)
    Private _LetraC1 As New SolidColorBrush(Colors.White)
    Private _TextID1 As String = "1"
    Private _FondoID1 As New SolidColorBrush(Colors.Green)
    Private _LetraID1 As New SolidColorBrush(Colors.White)
    Private _TextContador1 As String
    Private _TextAhora1 As String
    Private Horas1, Minutos1, Segundos1, MiliSegundos1 As Integer
    Private iniciado1 As Boolean = False
    Private dsp1 As New DispatcherTimer

    Private _TextBot2 As String = "Sin conexión"
    Private _FondoC2 As New SolidColorBrush(Colors.Transparent)
    Private _FondoD2 As New SolidColorBrush(Colors.Transparent)
    Private _LetraC2 As New SolidColorBrush(Colors.White)
    Private _TextID2 As String = "2"
    Private _FondoID2 As New SolidColorBrush(Colors.Green)
    Private _LetraID2 As New SolidColorBrush(Colors.White)
    Private _TextContador2 As String
    Private _TextAhora2 As String
    Private Horas2, Minutos2, Segundos2, MiliSegundos2 As Integer
    Private iniciado2 As Boolean = False
    Private dsp2 As New DispatcherTimer

    Private _TextBot3 As String = "Sin conexión"
    Private _FondoC3 As New SolidColorBrush(Colors.Transparent)
    Private _FondoD3 As New SolidColorBrush(Colors.Transparent)
    Private _LetraC3 As New SolidColorBrush(Colors.White)
    Private _TextID3 As String = "3"
    Private _FondoID3 As New SolidColorBrush(Colors.Green)
    Private _LetraID3 As New SolidColorBrush(Colors.White)
    Private _TextContador3 As String
    Private _TextAhora3 As String
    Private Horas3, Minutos3, Segundos3, MiliSegundos3 As Integer
    Private iniciado3 As Boolean = False
    Private dsp3 As New DispatcherTimer

    Private _TextBot4 As String = "Sin conexión"
    Private _FondoC4 As New SolidColorBrush(Colors.Transparent)
    Private _FondoD4 As New SolidColorBrush(Colors.Transparent)
    Private _LetraC4 As New SolidColorBrush(Colors.White)
    Private _TextID4 As String = "4"
    Private _FondoID4 As New SolidColorBrush(Colors.Green)
    Private _LetraID4 As New SolidColorBrush(Colors.White)
    Private _TextContador4 As String
    Private _TextAhora4 As String
    Private Horas4, Minutos4, Segundos4, MiliSegundos4 As Integer
    Private iniciado4 As Boolean = False
    Private dsp4 As New DispatcherTimer

    Private _TextBot5 As String = "Sin conexión"
    Private _FondoC5 As New SolidColorBrush(Colors.Transparent)
    Private _FondoD5 As New SolidColorBrush(Colors.Transparent)
    Private _LetraC5 As New SolidColorBrush(Colors.White)
    Private _TextID5 As String = "5"
    Private _FondoID5 As New SolidColorBrush(Colors.Green)
    Private _LetraID5 As New SolidColorBrush(Colors.White)
    Private _TextContador5 As String
    Private _TextAhora5 As String
    Private Horas5, Minutos5, Segundos5, MiliSegundos5 As Integer
    Private iniciado5 As Boolean = False
    Private dsp5 As New DispatcherTimer

    Private _TextBot6 As String = "Sin conexión"
    Private _FondoC6 As New SolidColorBrush(Colors.Transparent)
    Private _FondoD6 As New SolidColorBrush(Colors.Transparent)
    Private _LetraC6 As New SolidColorBrush(Colors.White)
    Private _TextID6 As String = "6"
    Private _FondoID6 As New SolidColorBrush(Colors.Green)
    Private _LetraID6 As New SolidColorBrush(Colors.White)
    Private _TextContador6 As String
    Private _TextAhora6 As String
    Private Horas6, Minutos6, Segundos6, MiliSegundos6 As Integer
    Private iniciado6 As Boolean = False
    Private dsp6 As New DispatcherTimer

    Private _TextBot7 As String = "Sin conexión"
    Private _FondoC7 As New SolidColorBrush(Colors.Transparent)
    Private _FondoD7 As New SolidColorBrush(Colors.Transparent)
    Private _LetraC7 As New SolidColorBrush(Colors.White)
    Private _TextID7 As String = "7"
    Private _FondoID7 As New SolidColorBrush(Colors.Green)
    Private _LetraID7 As New SolidColorBrush(Colors.White)
    Private _TextContador7 As String
    Private _TextAhora7 As String
    Private Horas7, Minutos7, Segundos7, MiliSegundos7 As Integer
    Private iniciado7 As Boolean = False
    Private dsp7 As New DispatcherTimer

    Private _TextBot8 As String = "Sin conexión"
    Private _FondoC8 As New SolidColorBrush(Colors.Transparent)
    Private _FondoD8 As New SolidColorBrush(Colors.Transparent)
    Private _LetraC8 As New SolidColorBrush(Colors.White)
    Private _TextID8 As String = "8"
    Private _FondoID8 As New SolidColorBrush(Colors.Green)
    Private _LetraID8 As New SolidColorBrush(Colors.White)
    Private _TextContador8 As String
    Private _TextAhora8 As String
    Private Horas8, Minutos8, Segundos8, MiliSegundos8 As Integer
    Private iniciado8 As Boolean = False
    Private dsp8 As New DispatcherTimer

    Private _TextBot9 As String = "Sin conexión"
    Private _FondoC9 As New SolidColorBrush(Colors.Transparent)
    Private _FondoD9 As New SolidColorBrush(Colors.Transparent)
    Private _LetraC9 As New SolidColorBrush(Colors.White)
    Private _TextID9 As String = "9"
    Private _FondoID9 As New SolidColorBrush(Colors.Green)
    Private _LetraID9 As New SolidColorBrush(Colors.White)
    Private _TextContador9 As String
    Private _TextAhora9 As String
    Private Horas9, Minutos9, Segundos9, MiliSegundos9 As Integer
    Private iniciado9 As Boolean = False
    Private dsp9 As New DispatcherTimer

    Private _TextBot10 As String = "Sin conexión"
    Private _FondoC10 As New SolidColorBrush(Colors.Transparent)
    Private _FondoD10 As New SolidColorBrush(Colors.Transparent)
    Private _LetraC10 As New SolidColorBrush(Colors.White)
    Private _TextID10 As String = "10"
    Private _FondoID10 As New SolidColorBrush(Colors.Green)
    Private _LetraID10 As New SolidColorBrush(Colors.White)
    Private _TextContador10 As String
    Private _TextAhora10 As String
    Private Horas10, Minutos10, Segundos10, MiliSegundos10 As Integer
    Private iniciado10 As Boolean = False
    Private dsp10 As New DispatcherTimer

    Private _TextBot11 As String = "Sin conexión"
    Private _FondoC11 As New SolidColorBrush(Colors.Transparent)
    Private _FondoD11 As New SolidColorBrush(Colors.Transparent)
    Private _LetraC11 As New SolidColorBrush(Colors.White)
    Private _TextID11 As String = "11"
    Private _FondoID11 As New SolidColorBrush(Colors.Green)
    Private _LetraID11 As New SolidColorBrush(Colors.White)
    Private _TextContador11 As String
    Private _TextAhora11 As String
    Private Horas11, Minutos11, Segundos11, MiliSegundos11 As Integer
    Private iniciado11 As Boolean = False
    Private dsp11 As New DispatcherTimer

    Private _TextBot12 As String = "Sin conexión"
    Private _FondoC12 As New SolidColorBrush(Colors.Transparent)
    Private _FondoD12 As New SolidColorBrush(Colors.Transparent)
    Private _LetraC12 As New SolidColorBrush(Colors.White)
    Private _TextID12 As String = "12"
    Private _FondoID12 As New SolidColorBrush(Colors.Green)
    Private _LetraID12 As New SolidColorBrush(Colors.White)
    Private _TextContador12 As String
    Private _TextAhora12 As String
    Private Horas12, Minutos12, Segundos12, MiliSegundos12 As Integer
    Private iniciado12 As Boolean = False
    Private dsp12 As New DispatcherTimer

    Private _TextBot13 As String = "Sin conexión"
    Private _FondoC13 As New SolidColorBrush(Colors.Transparent)
    Private _FondoD13 As New SolidColorBrush(Colors.Transparent)
    Private _LetraC13 As New SolidColorBrush(Colors.White)
    Private _TextID13 As String = "13"
    Private _FondoID13 As New SolidColorBrush(Colors.Green)
    Private _LetraID13 As New SolidColorBrush(Colors.White)
    Private _TextContador13 As String
    Private _TextAhora13 As String
    Private Horas13, Minutos13, Segundos13, MiliSegundos13 As Integer
    Private iniciado13 As Boolean = False
    Private dsp13 As New DispatcherTimer

    Private _TextBot14 As String = "Sin conexión"
    Private _FondoC14 As New SolidColorBrush(Colors.Transparent)
    Private _FondoD14 As New SolidColorBrush(Colors.Transparent)
    Private _LetraC14 As New SolidColorBrush(Colors.White)
    Private _TextID14 As String = "14"
    Private _FondoID14 As New SolidColorBrush(Colors.Green)
    Private _LetraID14 As New SolidColorBrush(Colors.White)
    Private _TextContador14 As String
    Private _TextAhora14 As String
    Private Horas14, Minutos14, Segundos14, MiliSegundos14 As Integer
    Private iniciado14 As Boolean = False
    Private dsp14 As New DispatcherTimer

    '------Singleton
    Private Shared _Instancia As ModeloVistaPrincipal = Nothing
    Private Shared ReadOnly _Sync As New Object

    Private Sub New()
    End Sub

    Public Shared ReadOnly Property Instancia() As ModeloVistaPrincipal
        Get
            If _Instancia Is Nothing Then
                SyncLock _Sync
                    If _Instancia Is Nothing Then
                        _Instancia = New ModeloVistaPrincipal()
                    End If
                End SyncLock
            End If
            Return _Instancia
        End Get
    End Property

    Public Sub Actu(ByRef _instance As MainWindow)
        Me._Main = _instance
    End Sub


    '------------------------------------------------FILA PRIMERA
    Public Property TextBot1 As String
        Get
            Return _TextBot1
        End Get
        Set(value As String)
            _TextBot1 = value
            NotificarCambio("TextBot1")
        End Set
    End Property

    Public Property FondoC1 As SolidColorBrush
        Get
            Return _FondoC1
        End Get
        Set(value As SolidColorBrush)
            _FondoC1 = value
            NotificarCambio("FondoC1")
        End Set
    End Property

    Public Property FondoD1 As SolidColorBrush
        Get
            Return _FondoD1
        End Get
        Set(value As SolidColorBrush)
            _FondoD1 = value
            NotificarCambio("FondoD1")
        End Set
    End Property

    Public Property LetraC1 As SolidColorBrush
        Get
            Return _LetraC1
        End Get
        Set(value As SolidColorBrush)
            _LetraC1 = value
            NotificarCambio("LetraC1")
        End Set
    End Property

    Public Property TextID1 As String
        Get
            Return _TextID1
        End Get
        Set(value As String)
            _TextID1 = value
            NotificarCambio("TextID1")
        End Set
    End Property

    Public Property FondoID1 As SolidColorBrush
        Get
            Return _FondoID1
        End Get
        Set(value As SolidColorBrush)
            _FondoID1 = value
            NotificarCambio("FondoID1")
        End Set
    End Property

    Public Property LetraID1 As SolidColorBrush
        Get
            Return _LetraID1
        End Get
        Set(value As SolidColorBrush)
            _LetraID1 = value
            NotificarCambio("LetraID1")
        End Set
    End Property

    Public Property TextContador1 As String
        Get
            Return _TextContador1
        End Get
        Set(value As String)
            _TextContador1 = value
            NotificarCambio("TextContador1")
        End Set
    End Property

    Public Property TextAhora1 As String
        Get
            Return _TextAhora1
        End Get
        Set(value As String)
            _TextAhora1 = value
            NotificarCambio("TextAhora1")
        End Set
    End Property

    '---------------------------------------------FILA SEGUNDA
    Public Property TextBot2 As String
        Get
            Return _TextBot2
        End Get
        Set(value As String)
            _TextBot2 = value
            NotificarCambio("TextBot2")
        End Set
    End Property

    Public Property FondoC2 As SolidColorBrush
        Get
            Return _FondoC2
        End Get
        Set(value As SolidColorBrush)
            _FondoC2 = value
            NotificarCambio("FondoC2")
        End Set
    End Property

    Public Property FondoD2 As SolidColorBrush
        Get
            Return _FondoD2
        End Get
        Set(value As SolidColorBrush)
            _FondoD2 = value
            NotificarCambio("FondoD2")
        End Set
    End Property

    Public Property LetraC2 As SolidColorBrush
        Get
            Return _LetraC2
        End Get
        Set(value As SolidColorBrush)
            _LetraC2 = value
            NotificarCambio("LetraC2")
        End Set
    End Property

    Public Property TextID2 As String
        Get
            Return _TextID2
        End Get
        Set(value As String)
            _TextID2 = value
            NotificarCambio("TextID2")
        End Set
    End Property

    Public Property FondoID2 As SolidColorBrush
        Get
            Return _FondoID2
        End Get
        Set(value As SolidColorBrush)
            _FondoID2 = value
            NotificarCambio("FondoID2")
        End Set
    End Property

    Public Property LetraID2 As SolidColorBrush
        Get
            Return _LetraID2
        End Get
        Set(value As SolidColorBrush)
            _LetraID2 = value
            NotificarCambio("LetraID2")
        End Set
    End Property

    Public Property TextContador2 As String
        Get
            Return _TextContador2
        End Get
        Set(value As String)
            _TextContador2 = value
            NotificarCambio("TextContador2")
        End Set
    End Property

    Public Property TextAhora2 As String
        Get
            Return _TextAhora2
        End Get
        Set(value As String)
            _TextAhora2 = value
            NotificarCambio("TextAhora2")
        End Set
    End Property

    '---------------------------------------------FILA TERCERA
    Public Property TextBot3 As String
        Get
            Return _TextBot3
        End Get
        Set(value As String)
            _TextBot3 = value
            NotificarCambio("TextBot3")
        End Set
    End Property

    Public Property FondoC3 As SolidColorBrush
        Get
            Return _FondoC3
        End Get
        Set(value As SolidColorBrush)
            _FondoC3 = value
            NotificarCambio("FondoC3")
        End Set
    End Property

    Public Property FondoD3 As SolidColorBrush
        Get
            Return _FondoD3
        End Get
        Set(value As SolidColorBrush)
            _FondoD3 = value
            NotificarCambio("FondoD3")
        End Set
    End Property

    Public Property LetraC3 As SolidColorBrush
        Get
            Return _LetraC3
        End Get
        Set(value As SolidColorBrush)
            _LetraC3 = value
            NotificarCambio("LetraC3")
        End Set
    End Property

    Public Property TextID3 As String
        Get
            Return _TextID3
        End Get
        Set(value As String)
            _TextAhora3 = value
            NotificarCambio("TextID3")
        End Set
    End Property

    Public Property FondoID3 As SolidColorBrush
        Get
            Return _FondoID3
        End Get
        Set(value As SolidColorBrush)
            _FondoC3 = value
            NotificarCambio("FondoID3")
        End Set
    End Property

    Public Property LetraID3 As SolidColorBrush
        Get
            Return _LetraID3
        End Get
        Set(value As SolidColorBrush)
            _LetraC3 = value
            NotificarCambio("LetraID3")
        End Set
    End Property

    Public Property TextContador3 As String
        Get
            Return _TextContador3
        End Get
        Set(value As String)
            _TextContador3 = value
            NotificarCambio("TextContador3")
        End Set
    End Property

    Public Property TextAhora3 As String
        Get
            Return _TextAhora3
        End Get
        Set(value As String)
            _TextAhora3 = value
            NotificarCambio("TextAhora3")
        End Set

    End Property

    '---------------------------------------------FILA CUARTA
    Public Property TextBot4 As String
        Get
            Return _TextBot4
        End Get
        Set(value As String)
            _TextBot4 = value
            NotificarCambio("TextBot4")
        End Set
    End Property

    Public Property FondoC4 As SolidColorBrush
        Get
            Return _FondoC4
        End Get
        Set(value As SolidColorBrush)
            _FondoC4 = value
            NotificarCambio("FondoC4")
        End Set
    End Property

    Public Property FondoD4 As SolidColorBrush
        Get
            Return _FondoD4
        End Get
        Set(value As SolidColorBrush)
            _FondoD4 = value
            NotificarCambio("FondoD4")
        End Set
    End Property

    Public Property LetraC4 As SolidColorBrush
        Get
            Return _LetraC4
        End Get
        Set(value As SolidColorBrush)
            _LetraC4 = value
            NotificarCambio("LetraC4")
        End Set
    End Property

    Public Property TextID4 As String
        Get
            Return _TextID4
        End Get
        Set(value As String)
            _TextAhora4 = value
            NotificarCambio("TextID4")
        End Set
    End Property

    Public Property FondoID4 As SolidColorBrush
        Get
            Return _FondoID4
        End Get
        Set(value As SolidColorBrush)
            _FondoC4 = value
            NotificarCambio("FondoID4")
        End Set
    End Property

    Public Property LetraID4 As SolidColorBrush
        Get
            Return _LetraID4
        End Get
        Set(value As SolidColorBrush)
            _LetraC4 = value
            NotificarCambio("LetraID4")
        End Set
    End Property

    Public Property TextContador4 As String
        Get
            Return _TextContador4
        End Get
        Set(value As String)
            _TextContador4 = value
            NotificarCambio("TextContador4")
        End Set
    End Property

    Public Property TextAhora4 As String
        Get
            Return _TextAhora4
        End Get
        Set(value As String)
            _TextAhora4 = value
            NotificarCambio("TextAhora4")
        End Set

    End Property

    '---------------------------------------------FILA QUINTA
    Public Property TextBot5 As String
        Get
            Return _TextBot5
        End Get
        Set(value As String)
            _TextBot5 = value
            NotificarCambio("TextBot5")
        End Set
    End Property

    Public Property FondoC5 As SolidColorBrush
        Get
            Return _FondoC5
        End Get
        Set(value As SolidColorBrush)
            _FondoC5 = value
            NotificarCambio("FondoC5")
        End Set
    End Property

    Public Property FondoD5 As SolidColorBrush
        Get
            Return _FondoD5
        End Get
        Set(value As SolidColorBrush)
            _FondoD5 = value
            NotificarCambio("FondoD5")
        End Set
    End Property

    Public Property LetraC5 As SolidColorBrush
        Get
            Return _LetraC5
        End Get
        Set(value As SolidColorBrush)
            _LetraC5 = value
            NotificarCambio("LetraC5")
        End Set
    End Property

    Public Property TextID5 As String
        Get
            Return _TextID5
        End Get
        Set(value As String)
            _TextAhora5 = value
            NotificarCambio("TextID5")
        End Set
    End Property

    Public Property FondoID5 As SolidColorBrush
        Get
            Return _FondoID5
        End Get
        Set(value As SolidColorBrush)
            _FondoC5 = value
            NotificarCambio("FondoID5")
        End Set
    End Property

    Public Property LetraID5 As SolidColorBrush
        Get
            Return _LetraID5
        End Get
        Set(value As SolidColorBrush)
            _LetraC5 = value
            NotificarCambio("LetraID5")
        End Set
    End Property

    Public Property TextContador5 As String
        Get
            Return _TextContador5
        End Get
        Set(value As String)
            _TextContador5 = value
            NotificarCambio("TextContador5")
        End Set
    End Property

    Public Property TextAhora5 As String
        Get
            Return _TextAhora5
        End Get
        Set(value As String)
            _TextAhora5 = value
            NotificarCambio("TextAhora5")
        End Set

    End Property

    '---------------------------------------------FILA SEXTA
    Public Property TextBot6 As String
        Get
            Return _TextBot6
        End Get
        Set(value As String)
            _TextBot6 = value
            NotificarCambio("TextBot6")
        End Set
    End Property

    Public Property FondoC6 As SolidColorBrush
        Get
            Return _FondoC6
        End Get
        Set(value As SolidColorBrush)
            _FondoC6 = value
            NotificarCambio("FondoC6")
        End Set
    End Property

    Public Property FondoD6 As SolidColorBrush
        Get
            Return _FondoD6
        End Get
        Set(value As SolidColorBrush)
            _FondoD6 = value
            NotificarCambio("FondoD6")
        End Set
    End Property

    Public Property LetraC6 As SolidColorBrush
        Get
            Return _LetraC6
        End Get
        Set(value As SolidColorBrush)
            _LetraC6 = value
            NotificarCambio("LetraC6")
        End Set
    End Property

    Public Property TextID6 As String
        Get
            Return _TextID6
        End Get
        Set(value As String)
            _TextAhora6 = value
            NotificarCambio("TextID6")
        End Set
    End Property

    Public Property FondoID6 As SolidColorBrush
        Get
            Return _FondoID6
        End Get
        Set(value As SolidColorBrush)
            _FondoC6 = value
            NotificarCambio("FondoID6")
        End Set
    End Property

    Public Property LetraID6 As SolidColorBrush
        Get
            Return _LetraID6
        End Get
        Set(value As SolidColorBrush)
            _LetraC6 = value
            NotificarCambio("LetraID6")
        End Set
    End Property

    Public Property TextContador6 As String
        Get
            Return _TextContador6
        End Get
        Set(value As String)
            _TextContador6 = value
            NotificarCambio("TextContador6")
        End Set
    End Property

    Public Property TextAhora6 As String
        Get
            Return _TextAhora6
        End Get
        Set(value As String)
            _TextAhora6 = value
            NotificarCambio("TextAhora6")
        End Set

    End Property

    '---------------------------------------------FILA SEPTIMA
    Public Property TextBot7 As String
        Get
            Return _TextBot7
        End Get
        Set(value As String)
            _TextBot7 = value
            NotificarCambio("TextBot7")
        End Set
    End Property

    Public Property FondoC7 As SolidColorBrush
        Get
            Return _FondoC7
        End Get
        Set(value As SolidColorBrush)
            _FondoC7 = value
            NotificarCambio("FondoC7")
        End Set
    End Property

    Public Property FondoD7 As SolidColorBrush
        Get
            Return _FondoD7
        End Get
        Set(value As SolidColorBrush)
            _FondoD7 = value
            NotificarCambio("FondoD7")
        End Set
    End Property

    Public Property LetraC7 As SolidColorBrush
        Get
            Return _LetraC7
        End Get
        Set(value As SolidColorBrush)
            _LetraC7 = value
            NotificarCambio("LetraC7")
        End Set
    End Property

    Public Property TextID7 As String
        Get
            Return _TextID7
        End Get
        Set(value As String)
            _TextAhora7 = value
            NotificarCambio("TextID7")
        End Set
    End Property

    Public Property FondoID7 As SolidColorBrush
        Get
            Return _FondoID7
        End Get
        Set(value As SolidColorBrush)
            _FondoC7 = value
            NotificarCambio("FondoID7")
        End Set
    End Property

    Public Property LetraID7 As SolidColorBrush
        Get
            Return _LetraID7
        End Get
        Set(value As SolidColorBrush)
            _LetraC7 = value
            NotificarCambio("LetraID7")
        End Set
    End Property

    Public Property TextContador7 As String
        Get
            Return _TextContador7
        End Get
        Set(value As String)
            _TextContador7 = value
            NotificarCambio("TextContador7")
        End Set
    End Property

    Public Property TextAhora7 As String
        Get
            Return _TextAhora7
        End Get
        Set(value As String)
            _TextAhora7 = value
            NotificarCambio("TextAhora7")
        End Set

    End Property

    '---------------------------------------------FILA OCTAVA
    Public Property TextBot8 As String
        Get
            Return _TextBot8
        End Get
        Set(value As String)
            _TextBot8 = value
            NotificarCambio("TextBot8")
        End Set
    End Property

    Public Property FondoC8 As SolidColorBrush
        Get
            Return _FondoC8
        End Get
        Set(value As SolidColorBrush)
            _FondoC8 = value
            NotificarCambio("FondoC8")
        End Set
    End Property

    Public Property FondoD8 As SolidColorBrush
        Get
            Return _FondoD8
        End Get
        Set(value As SolidColorBrush)
            _FondoD8 = value
            NotificarCambio("FondoD8")
        End Set
    End Property

    Public Property LetraC8 As SolidColorBrush
        Get
            Return _LetraC8
        End Get
        Set(value As SolidColorBrush)
            _LetraC8 = value
            NotificarCambio("LetraC8")
        End Set
    End Property

    Public Property TextID8 As String
        Get
            Return _TextID8
        End Get
        Set(value As String)
            _TextAhora8 = value
            NotificarCambio("TextID8")
        End Set
    End Property

    Public Property FondoID8 As SolidColorBrush
        Get
            Return _FondoID8
        End Get
        Set(value As SolidColorBrush)
            _FondoC8 = value
            NotificarCambio("FondoID8")
        End Set
    End Property

    Public Property LetraID8 As SolidColorBrush
        Get
            Return _LetraID8
        End Get
        Set(value As SolidColorBrush)
            _LetraC8 = value
            NotificarCambio("LetraID8")
        End Set
    End Property

    Public Property TextContador8 As String
        Get
            Return _TextContador8
        End Get
        Set(value As String)
            _TextContador8 = value
            NotificarCambio("TextContador8")
        End Set
    End Property

    Public Property TextAhora8 As String
        Get
            Return _TextAhora8
        End Get
        Set(value As String)
            _TextAhora8 = value
            NotificarCambio("TextAhora8")
        End Set

    End Property

    '---------------------------------------------FILA NOVENA
    Public Property TextBot9 As String
        Get
            Return _TextBot9
        End Get
        Set(value As String)
            _TextBot9 = value
            NotificarCambio("TextBot9")
        End Set
    End Property

    Public Property FondoC9 As SolidColorBrush
        Get
            Return _FondoC9
        End Get
        Set(value As SolidColorBrush)
            _FondoC9 = value
            NotificarCambio("FondoC9")
        End Set
    End Property

    Public Property FondoD9 As SolidColorBrush
        Get
            Return _FondoD9
        End Get
        Set(value As SolidColorBrush)
            _FondoD9 = value
            NotificarCambio("FondoD9")
        End Set
    End Property

    Public Property LetraC9 As SolidColorBrush
        Get
            Return _LetraC9
        End Get
        Set(value As SolidColorBrush)
            _LetraC9 = value
            NotificarCambio("LetraC9")
        End Set
    End Property

    Public Property TextID9 As String
        Get
            Return _TextID9
        End Get
        Set(value As String)
            _TextAhora9 = value
            NotificarCambio("TextID9")
        End Set
    End Property

    Public Property FondoID9 As SolidColorBrush
        Get
            Return _FondoID9
        End Get
        Set(value As SolidColorBrush)
            _FondoC9 = value
            NotificarCambio("FondoID9")
        End Set
    End Property

    Public Property LetraID9 As SolidColorBrush
        Get
            Return _LetraID9
        End Get
        Set(value As SolidColorBrush)
            _LetraC9 = value
            NotificarCambio("LetraID9")
        End Set
    End Property

    Public Property TextContador9 As String
        Get
            Return _TextContador9
        End Get
        Set(value As String)
            _TextContador9 = value
            NotificarCambio("TextContador9")
        End Set
    End Property

    Public Property TextAhora9 As String
        Get
            Return _TextAhora9
        End Get
        Set(value As String)
            _TextAhora9 = value
            NotificarCambio("TextAhora9")
        End Set

    End Property

    '---------------------------------------------FILA DECIMA
    Public Property TextBot10 As String
        Get
            Return _TextBot10
        End Get
        Set(value As String)
            _TextBot10 = value
            NotificarCambio("TextBot10")
        End Set
    End Property

    Public Property FondoC10 As SolidColorBrush
        Get
            Return _FondoC10
        End Get
        Set(value As SolidColorBrush)
            _FondoC10 = value
            NotificarCambio("FondoC10")
        End Set
    End Property

    Public Property FondoD10 As SolidColorBrush
        Get
            Return _FondoD10
        End Get
        Set(value As SolidColorBrush)
            _FondoD10 = value
            NotificarCambio("FondoD10")
        End Set
    End Property

    Public Property LetraC10 As SolidColorBrush
        Get
            Return _LetraC10
        End Get
        Set(value As SolidColorBrush)
            _LetraC10 = value
            NotificarCambio("LetraC10")
        End Set
    End Property

    Public Property TextID10 As String
        Get
            Return _TextID10
        End Get
        Set(value As String)
            _TextAhora10 = value
            NotificarCambio("TextID10")
        End Set
    End Property

    Public Property FondoID10 As SolidColorBrush
        Get
            Return _FondoID10
        End Get
        Set(value As SolidColorBrush)
            _FondoC10 = value
            NotificarCambio("FondoID10")
        End Set
    End Property

    Public Property LetraID10 As SolidColorBrush
        Get
            Return _LetraID10
        End Get
        Set(value As SolidColorBrush)
            _LetraC10 = value
            NotificarCambio("LetraID10")
        End Set
    End Property

    Public Property TextContador10 As String
        Get
            Return _TextContador10
        End Get
        Set(value As String)
            _TextContador10 = value
            NotificarCambio("TextContador10")
        End Set
    End Property

    Public Property TextAhora10 As String
        Get
            Return _TextAhora10
        End Get
        Set(value As String)
            _TextAhora10 = value
            NotificarCambio("TextAhora10")
        End Set
    End Property

    '---------------------------------------------FILA DECIMOPRIMERA
    Public Property TextBot11 As String
        Get
            Return _TextBot11
        End Get
        Set(value As String)
            _TextBot11 = value
            NotificarCambio("TextBot11")
        End Set
    End Property

    Public Property FondoC11 As SolidColorBrush
        Get
            Return _FondoC11
        End Get
        Set(value As SolidColorBrush)
            _FondoC11 = value
            NotificarCambio("FondoC11")
        End Set
    End Property

    Public Property FondoD11 As SolidColorBrush
        Get
            Return _FondoD11
        End Get
        Set(value As SolidColorBrush)
            _FondoD11 = value
            NotificarCambio("FondoD11")
        End Set
    End Property

    Public Property LetraC11 As SolidColorBrush
        Get
            Return _LetraC11
        End Get
        Set(value As SolidColorBrush)
            _LetraC11 = value
            NotificarCambio("LetraC11")
        End Set
    End Property

    Public Property TextID11 As String
        Get
            Return _TextID11
        End Get
        Set(value As String)
            _TextAhora11 = value
            NotificarCambio("TextID11")
        End Set
    End Property

    Public Property FondoID11 As SolidColorBrush
        Get
            Return _FondoID11
        End Get
        Set(value As SolidColorBrush)
            _FondoC11 = value
            NotificarCambio("FondoID11")
        End Set
    End Property

    Public Property LetraID11 As SolidColorBrush
        Get
            Return _LetraID11
        End Get
        Set(value As SolidColorBrush)
            _LetraC11 = value
            NotificarCambio("LetraID11")
        End Set
    End Property

    Public Property TextContador11 As String
        Get
            Return _TextContador11
        End Get
        Set(value As String)
            _TextContador11 = value
            NotificarCambio("TextContador11")
        End Set
    End Property

    Public Property TextAhora11 As String
        Get
            Return _TextAhora11
        End Get
        Set(value As String)
            _TextAhora11 = value
            NotificarCambio("TextAhora11")
        End Set
    End Property

    '---------------------------------------------FILA DECIMOSEGUNDA
    Public Property TextBot12 As String
        Get
            Return _TextBot12
        End Get
        Set(value As String)
            _TextBot12 = value
            NotificarCambio("TextBot12")
        End Set
    End Property

    Public Property FondoC12 As SolidColorBrush
        Get
            Return _FondoC12
        End Get
        Set(value As SolidColorBrush)
            _FondoC12 = value
            NotificarCambio("FondoC12")
        End Set
    End Property

    Public Property FondoD12 As SolidColorBrush
        Get
            Return _FondoD12
        End Get
        Set(value As SolidColorBrush)
            _FondoD12 = value
            NotificarCambio("FondoD12")
        End Set
    End Property

    Public Property LetraC12 As SolidColorBrush
        Get
            Return _LetraC12
        End Get
        Set(value As SolidColorBrush)
            _LetraC12 = value
            NotificarCambio("LetraC12")
        End Set
    End Property

    Public Property TextID12 As String
        Get
            Return _TextID12
        End Get
        Set(value As String)
            _TextAhora12 = value
            NotificarCambio("TextID12")
        End Set
    End Property

    Public Property FondoID12 As SolidColorBrush
        Get
            Return _FondoID12
        End Get
        Set(value As SolidColorBrush)
            _FondoC12 = value
            NotificarCambio("FondoID12")
        End Set
    End Property

    Public Property LetraID12 As SolidColorBrush
        Get
            Return _LetraID12
        End Get
        Set(value As SolidColorBrush)
            _LetraC12 = value
            NotificarCambio("LetraID12")
        End Set
    End Property

    Public Property TextContador12 As String
        Get
            Return _TextContador12
        End Get
        Set(value As String)
            _TextContador12 = value
            NotificarCambio("TextContador12")
        End Set
    End Property

    Public Property TextAhora12 As String
        Get
            Return _TextAhora12
        End Get
        Set(value As String)
            _TextAhora12 = value
            NotificarCambio("TextAhora12")
        End Set
    End Property

    '---------------------------------------------FILA DECIMO TERCERA
    Public Property TextBot13 As String
        Get
            Return _TextBot13
        End Get
        Set(value As String)
            _TextBot13 = value
            NotificarCambio("TextBot13")
        End Set
    End Property

    Public Property FondoC13 As SolidColorBrush
        Get
            Return _FondoC13
        End Get
        Set(value As SolidColorBrush)
            _FondoC13 = value
            NotificarCambio("FondoC13")
        End Set
    End Property

    Public Property FondoD13 As SolidColorBrush
        Get
            Return _FondoD13
        End Get
        Set(value As SolidColorBrush)
            _FondoD13 = value
            NotificarCambio("FondoD13")
        End Set
    End Property

    Public Property LetraC13 As SolidColorBrush
        Get
            Return _LetraC13
        End Get
        Set(value As SolidColorBrush)
            _LetraC13 = value
            NotificarCambio("LetraC13")
        End Set
    End Property

    Public Property TextID13 As String
        Get
            Return _TextID13
        End Get
        Set(value As String)
            _TextAhora13 = value
            NotificarCambio("TextID13")
        End Set
    End Property

    Public Property FondoID13 As SolidColorBrush
        Get
            Return _FondoID13
        End Get
        Set(value As SolidColorBrush)
            _FondoC13 = value
            NotificarCambio("FondoID13")
        End Set
    End Property

    Public Property LetraID13 As SolidColorBrush
        Get
            Return _LetraID13
        End Get
        Set(value As SolidColorBrush)
            _LetraC13 = value
            NotificarCambio("LetraID13")
        End Set
    End Property

    Public Property TextContador13 As String
        Get
            Return _TextContador13
        End Get
        Set(value As String)
            _TextContador13 = value
            NotificarCambio("TextContador13")
        End Set
    End Property

    Public Property TextAhora13 As String
        Get
            Return _TextAhora13
        End Get
        Set(value As String)
            _TextAhora13 = value
            NotificarCambio("TextAhora13")
        End Set
    End Property

    '---------------------------------------------FILA DECIMO CUARTA
    Public Property TextBot14 As String
        Get
            Return _TextBot14
        End Get
        Set(value As String)
            _TextBot14 = value
            NotificarCambio("TextBot14")
        End Set
    End Property

    Public Property FondoC14 As SolidColorBrush
        Get
            Return _FondoC14
        End Get
        Set(value As SolidColorBrush)
            _FondoC14 = value
            NotificarCambio("FondoC14")
        End Set
    End Property

    Public Property FondoD14 As SolidColorBrush
        Get
            Return _FondoD14
        End Get
        Set(value As SolidColorBrush)
            _FondoD14 = value
            NotificarCambio("FondoD14")
        End Set
    End Property

    Public Property LetraC14 As SolidColorBrush
        Get
            Return _LetraC14
        End Get
        Set(value As SolidColorBrush)
            _LetraC14 = value
            NotificarCambio("LetraC14")
        End Set
    End Property

    Public Property TextID14 As String
        Get
            Return _TextID14
        End Get
        Set(value As String)
            _TextAhora14 = value
            NotificarCambio("TextID14")
        End Set
    End Property

    Public Property FondoID14 As SolidColorBrush
        Get
            Return _FondoID14
        End Get
        Set(value As SolidColorBrush)
            _FondoC14 = value
            NotificarCambio("FondoID14")
        End Set
    End Property

    Public Property LetraID14 As SolidColorBrush
        Get
            Return _LetraID14
        End Get
        Set(value As SolidColorBrush)
            _LetraC14 = value
            NotificarCambio("LetraID14")
        End Set
    End Property

    Public Property TextContador14 As String
        Get
            Return _TextContador14
        End Get
        Set(value As String)
            _TextContador14 = value
            NotificarCambio("TextContador14")
        End Set
    End Property

    Public Property TextAhora14 As String
        Get
            Return _TextAhora14
        End Get
        Set(value As String)
            _TextAhora14 = value
            NotificarCambio("TextAhora14")
        End Set
    End Property

    '----------- MÉTODOS
    Public Sub NotificarCambio(ByVal Propiedad As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(Propiedad))
    End Sub

    Public Event CanExecuteChanged As EventHandler Implements ICommand.CanExecuteChanged
    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

    Public Function CanExecute(parameter As Object) As Boolean Implements ICommand.CanExecute
        Return True
    End Function

    Public Sub Limpiar1()
        Dim Rojo As New SolidColorBrush(Colors.Red)
        Dim Verde As New SolidColorBrush(Colors.Green)
        Dim Nada As New SolidColorBrush(Colors.Transparent)
        Dim Blanco As New SolidColorBrush(Colors.White)
        Dim Gris As New SolidColorBrush(Colors.DarkGray)
        Dim Negro As New SolidColorBrush(Colors.Black)

        _FondoID1 = Verde
        _LetraID1 = Blanco
        _TextBot1 = "Sin conexión"
        _FondoC1 = Nada
        _FondoD1 = Nada
        _LetraC1 = Blanco
        contar = 0

        iniciado1 = True
        Horas1 = 0
        Minutos1 = 0
        Segundos1 = 0
        MiliSegundos1 = 0
        _TextContador1 = ""
        _TextAhora1 = ""

        NotificarCambio("FondoID1")
        NotificarCambio("TextAhora1")
        NotificarCambio("LetraID1")
        NotificarCambio("TextContador1")
        NotificarCambio("TextBot1")
        NotificarCambio("FondoD1")
        NotificarCambio("FondoC1")
        NotificarCambio("LetraC1")
    End Sub

    Public Sub Seguir1()
        Dim Rojo As New SolidColorBrush(Colors.Red)
        Dim Verde As New SolidColorBrush(Colors.Green)
        Dim Nada As New SolidColorBrush(Colors.Transparent)
        Dim Blanco As New SolidColorBrush(Colors.White)
        Dim Gris As New SolidColorBrush(Colors.DarkGray)
        Dim Negro As New SolidColorBrush(Colors.Black)

        dsp1.Start()
        _FondoID1 = Rojo
        _TextBot1 = "Conectado"
        _FondoC1 = Gris
        _FondoD1 = Gris
        _LetraID1 = Blanco
        _LetraC1 = Blanco

        NotificarCambio("FondoID1")
        NotificarCambio("TextBot1")
        NotificarCambio("FondoC1")
        NotificarCambio("FondoD1")
        NotificarCambio("LetraC1")
        NotificarCambio("LetraID1")
    End Sub

    Public Sub Execute(Parametro As Object) Implements ICommand.Execute

        Dim Rojo As New SolidColorBrush(Colors.Red)
        Dim Verde As New SolidColorBrush(Colors.Green)
        Dim Nada As New SolidColorBrush(Colors.Transparent)
        Dim Blanco As New SolidColorBrush(Colors.White)
        Dim Gris As New SolidColorBrush(Colors.DarkGray)
        Dim Negro As New SolidColorBrush(Colors.Black)

        Select Case Parametro

            Case "BEstado1"
                If contar = 0 Then
                    If iniciado1 = False Then
                        dsp1.Interval = New TimeSpan(0, 0, 0, 0, 9)
                        AddHandler dsp1.Tick, AddressOf intervalo
                        dsp1.Start()
                        iniciado1 = True
                    Else
                        dsp1.Start()
                    End If
                    _FondoID1 = Rojo
                    _TextBot1 = "Conectado"
                    _FondoC1 = Gris
                    _FondoD1 = Gris
                    _LetraID1 = Blanco
                    _LetraC1 = Blanco
                    _TextAhora1 = TimeString
                    contar = 1

                    NotificarCambio("FondoID1")
                    NotificarCambio("TextAhora1")
                    NotificarCambio("TextBot1")
                    NotificarCambio("FondoC1")
                    NotificarCambio("FondoD1")
                    NotificarCambio("LetraC1")
                    NotificarCambio("LetraID1")

                ElseIf contar = 1 Then
                    contar = 3
                    _TextContador1 = Format(Horas1, "00") & ":" & Format(Minutos1, "00") & ":" & Format(Segundos1, "00") & "." & Format(MiliSegundos1, "00")
                    dsp1.Stop()
                    _TextBot1 = "Cobrando"
                    _LetraC1 = Negro
                    _LetraID1 = Negro
                    _FondoD1 = Rojo
                    _FondoID1 = Gris


                    NotificarCambio("FondoID1")
                    NotificarCambio("FondoD1")
                    NotificarCambio("LetraC1")
                    NotificarCambio("LetraID1")
                    NotificarCambio("TextContador1")
                    NotificarCambio("TextBot1")

                    '------------------------------------------------
                    Dim _Cobrando As New Cobrar
                    _Cobrando.Show()

                ElseIf contar = 3 Then
                    dsp1.Stop()
                    _TextBot1 = "Cobrando"
                    _LetraC1 = Negro
                    _LetraID1 = Negro
                    _FondoD1 = Rojo
                    _FondoID1 = Gris


                    NotificarCambio("FondoID1")
                    NotificarCambio("FondoD1")
                    NotificarCambio("LetraC1")
                    NotificarCambio("LetraID1")
                    NotificarCambio("TextContador1")
                    NotificarCambio("TextBot1")

                    Dim _Cobrando As New Cobrar
                    _Cobrando.Show()

                End If

            Case "BEstado2"
                If contar2 = 0 Then
                    If iniciado2 = False Then
                        dsp2.Interval = New TimeSpan(0, 0, 0, 0, 9)
                        AddHandler dsp2.Tick, AddressOf intervalo2
                        dsp2.Start()
                        iniciado2 = True
                    Else
                        dsp2.Start()
                    End If
                    _FondoID2 = Rojo
                    _TextBot2 = "Conectado"
                    _FondoC2 = Gris
                    _FondoD2 = Gris
                    _LetraID2 = Blanco
                    _LetraC2 = Blanco
                    _TextAhora2 = TimeString
                    contar2 = 1

                    NotificarCambio("FondoID2")
                    NotificarCambio("TextAhora2")
                    NotificarCambio("TextBot2")
                    NotificarCambio("FondoC2")
                    NotificarCambio("FondoD2")
                    NotificarCambio("LetraC2")
                    NotificarCambio("LetraID2")

                ElseIf contar2 = 1 Then
                    _TextContador2 = Format(Horas2, "00") & ":" & Format(Minutos2, "00") & ":" & Format(Segundos2, "00") & "." & Format(MiliSegundos2, "00")
                    dsp2.Stop()
                    _TextBot2 = "Cobrando"
                    _LetraC2 = Negro
                    _LetraID2 = Negro
                    _FondoD2 = Rojo
                    _FondoID2 = Gris
                    contar2 = 2

                    NotificarCambio("FondoID2")
                    NotificarCambio("FondoD2")
                    NotificarCambio("LetraC2")
                    NotificarCambio("LetraID2")
                    NotificarCambio("TextContador2")
                    NotificarCambio("TextBot2")
                ElseIf contar2 = 2 Then
                    _FondoID2 = Verde
                    _LetraID2 = Blanco
                    _TextBot2 = "Sin conexión"
                    _FondoC2 = Nada
                    _FondoD2 = Nada
                    _LetraC2 = Blanco
                    contar2 = 0

                    iniciado2 = True
                    Horas2 = 0
                    Minutos2 = 0
                    Segundos2 = 0
                    MiliSegundos2 = 0
                    _TextContador2 = ""
                    _TextAhora2 = ""

                    NotificarCambio("FondoID2")
                    NotificarCambio("TextAhora2")
                    NotificarCambio("LetraID2")
                    NotificarCambio("TextContador2")
                    NotificarCambio("TextBot2")
                    NotificarCambio("FondoD2")
                    NotificarCambio("FondoC2")
                    NotificarCambio("LetraC2")
                End If

            Case "BEstado3"
                If contar3 = 0 Then
                    If iniciado3 = False Then
                        dsp3.Interval = New TimeSpan(0, 0, 0, 0, 9)
                        AddHandler dsp3.Tick, AddressOf intervalo3
                        dsp3.Start()
                        iniciado3 = True
                    Else
                        dsp3.Start()
                    End If
                    _FondoID3 = Rojo
                    _TextBot3 = "Conectado"
                    _FondoC3 = Gris
                    _FondoD3 = Gris
                    _LetraID3 = Blanco
                    _LetraC3 = Blanco
                    _TextAhora3 = TimeString
                    contar3 = 1

                    NotificarCambio("FondoID3")
                    NotificarCambio("TextAhora3")
                    NotificarCambio("TextBot3")
                    NotificarCambio("FondoC3")
                    NotificarCambio("FondoD3")
                    NotificarCambio("LetraC3")
                    NotificarCambio("LetraID3")

                ElseIf contar3 = 1 Then
                    _TextContador3 = Format(Horas3, "00") & ":" & Format(Minutos3, "00") & ":" & Format(Segundos3, "00") & "." & Format(MiliSegundos3, "00")
                    dsp3.Stop()
                    _TextBot3 = "Cobrando"
                    _LetraC3 = Negro
                    _LetraID3 = Negro
                    _FondoD3 = Rojo
                    _FondoID3 = Gris
                    contar3 = 2

                    NotificarCambio("FondoID3")
                    NotificarCambio("FondoD3")
                    NotificarCambio("LetraC3")
                    NotificarCambio("LetraID3")
                    NotificarCambio("TextContador3")
                    NotificarCambio("TextBot3")
                ElseIf contar3 = 2 Then
                    _FondoID3 = Verde
                    _LetraID3 = Blanco
                    _TextBot3 = "Sin conexión"
                    _FondoC3 = Nada
                    _FondoD3 = Nada
                    _LetraC3 = Blanco
                    contar3 = 0

                    iniciado3 = True
                    Horas3 = 0
                    Minutos3 = 0
                    Segundos3 = 0
                    MiliSegundos3 = 0
                    _TextContador3 = ""
                    _TextAhora3 = ""

                    NotificarCambio("FondoID3")
                    NotificarCambio("TextAhora3")
                    NotificarCambio("LetraID3")
                    NotificarCambio("TextContador3")
                    NotificarCambio("TextBot3")
                    NotificarCambio("FondoD3")
                    NotificarCambio("FondoC3")
                    NotificarCambio("LetraC3")
                End If

            Case "BEstado4"
                If contar4 = 0 Then
                    If iniciado4 = False Then
                        dsp4.Interval = New TimeSpan(0, 0, 0, 0, 9)
                        AddHandler dsp4.Tick, AddressOf intervalo4
                        dsp4.Start()
                        iniciado4 = True
                    Else
                        dsp4.Start()
                    End If
                    _FondoID4 = Rojo
                    _TextBot4 = "Conectado"
                    _FondoC4 = Gris
                    _FondoD4 = Gris
                    _LetraID4 = Blanco
                    _LetraC4 = Blanco
                    _TextAhora4 = TimeString
                    contar4 = 1

                    NotificarCambio("FondoID4")
                    NotificarCambio("TextAhora4")
                    NotificarCambio("TextBot4")
                    NotificarCambio("FondoC4")
                    NotificarCambio("FondoD4")
                    NotificarCambio("LetraC4")
                    NotificarCambio("LetraID4")

                ElseIf contar4 = 1 Then
                    _TextContador4 = Format(Horas4, "00") & ":" & Format(Minutos4, "00") & ":" & Format(Segundos4, "00") & "." & Format(MiliSegundos4, "00")
                    dsp4.Stop()
                    _TextBot4 = "Cobrando"
                    _LetraC4 = Negro
                    _LetraID4 = Negro
                    _FondoD4 = Rojo
                    _FondoID4 = Gris
                    contar4 = 2

                    NotificarCambio("FondoID4")
                    NotificarCambio("FondoD4")
                    NotificarCambio("LetraC4")
                    NotificarCambio("LetraID4")
                    NotificarCambio("TextContador4")
                    NotificarCambio("TextBot4")
                ElseIf contar4 = 2 Then
                    _FondoID4 = Verde
                    _LetraID4 = Blanco
                    _TextBot4 = "Sin conexión"
                    _FondoC4 = Nada
                    _FondoD4 = Nada
                    _LetraC4 = Blanco
                    contar4 = 0

                    iniciado4 = True
                    Horas4 = 0
                    Minutos4 = 0
                    Segundos4 = 0
                    MiliSegundos4 = 0
                    _TextContador4 = ""
                    _TextAhora4 = ""

                    NotificarCambio("FondoID4")
                    NotificarCambio("TextAhora4")
                    NotificarCambio("LetraID4")
                    NotificarCambio("TextContador4")
                    NotificarCambio("TextBot4")
                    NotificarCambio("FondoD4")
                    NotificarCambio("FondoC4")
                    NotificarCambio("LetraC4")
                End If

            Case "BEstado5"
                If contar5 = 0 Then
                    If iniciado5 = False Then
                        dsp5.Interval = New TimeSpan(0, 0, 0, 0, 9)
                        AddHandler dsp5.Tick, AddressOf intervalo5
                        dsp5.Start()
                        iniciado5 = True
                    Else
                        dsp5.Start()
                    End If
                    _FondoID5 = Rojo
                    _TextBot5 = "Conectado"
                    _FondoC5 = Gris
                    _FondoD5 = Gris
                    _LetraID5 = Blanco
                    _LetraC5 = Blanco
                    _TextAhora5 = TimeString
                    contar5 = 1

                    NotificarCambio("FondoID5")
                    NotificarCambio("TextAhora5")
                    NotificarCambio("TextBot5")
                    NotificarCambio("FondoC5")
                    NotificarCambio("FondoD5")
                    NotificarCambio("LetraC5")
                    NotificarCambio("LetraID5")

                ElseIf contar5 = 1 Then
                    _TextContador5 = Format(Horas5, "00") & ":" & Format(Minutos5, "00") & ":" & Format(Segundos5, "00") & "." & Format(MiliSegundos5, "00")
                    dsp5.Stop()
                    _TextBot5 = "Cobrando"
                    _LetraC5 = Negro
                    _LetraID5 = Negro
                    _FondoD5 = Rojo
                    _FondoID5 = Gris
                    contar5 = 2

                    NotificarCambio("FondoID5")
                    NotificarCambio("FondoD5")
                    NotificarCambio("LetraC5")
                    NotificarCambio("LetraID5")
                    NotificarCambio("TextContador5")
                    NotificarCambio("TextBot5")
                ElseIf contar5 = 2 Then
                    _FondoID5 = Verde
                    _LetraID5 = Blanco
                    _TextBot5 = "Sin conexión"
                    _FondoC5 = Nada
                    _FondoD5 = Nada
                    _LetraC5 = Blanco
                    contar5 = 0

                    iniciado5 = True
                    Horas5 = 0
                    Minutos5 = 0
                    Segundos5 = 0
                    MiliSegundos5 = 0
                    _TextContador5 = ""
                    _TextAhora5 = ""

                    NotificarCambio("FondoID5")
                    NotificarCambio("TextAhora5")
                    NotificarCambio("LetraID5")
                    NotificarCambio("TextContador5")
                    NotificarCambio("TextBot5")
                    NotificarCambio("FondoD5")
                    NotificarCambio("FondoC5")
                    NotificarCambio("LetraC5")
                End If

            Case "BEstado6"
                If contar6 = 0 Then
                    If iniciado6 = False Then
                        dsp6.Interval = New TimeSpan(0, 0, 0, 0, 9)
                        AddHandler dsp6.Tick, AddressOf intervalo6
                        dsp6.Start()
                        iniciado6 = True
                    Else
                        dsp6.Start()
                    End If
                    _FondoID6 = Rojo
                    _TextBot6 = "Conectado"
                    _FondoC6 = Gris
                    _FondoD6 = Gris
                    _LetraID6 = Blanco
                    _LetraC6 = Blanco
                    _TextAhora6 = TimeString
                    contar6 = 1

                    NotificarCambio("FondoID6")
                    NotificarCambio("TextAhora6")
                    NotificarCambio("TextBot6")
                    NotificarCambio("FondoC6")
                    NotificarCambio("FondoD6")
                    NotificarCambio("LetraC6")
                    NotificarCambio("LetraID6")

                ElseIf contar6 = 1 Then
                    _TextContador6 = Format(Horas6, "00") & ":" & Format(Minutos6, "00") & ":" & Format(Segundos6, "00") & "." & Format(MiliSegundos6, "00")
                    dsp6.Stop()
                    _TextBot6 = "Cobrando"
                    _LetraC6 = Negro
                    _LetraID6 = Negro
                    _FondoD6 = Rojo
                    _FondoID6 = Gris
                    contar6 = 2

                    NotificarCambio("FondoID6")
                    NotificarCambio("FondoD6")
                    NotificarCambio("LetraC6")
                    NotificarCambio("LetraID6")
                    NotificarCambio("TextContador6")
                    NotificarCambio("TextBot6")
                ElseIf contar6 = 2 Then
                    _FondoID6 = Verde
                    _LetraID6 = Blanco
                    _TextBot6 = "Sin conexión"
                    _FondoC6 = Nada
                    _FondoD6 = Nada
                    _LetraC6 = Blanco
                    contar6 = 0

                    iniciado6 = True
                    Horas6 = 0
                    Minutos6 = 0
                    Segundos6 = 0
                    MiliSegundos6 = 0
                    _TextContador6 = ""
                    _TextAhora6 = ""

                    NotificarCambio("FondoID6")
                    NotificarCambio("TextAhora6")
                    NotificarCambio("LetraID6")
                    NotificarCambio("TextContador6")
                    NotificarCambio("TextBot6")
                    NotificarCambio("FondoD6")
                    NotificarCambio("FondoC6")
                    NotificarCambio("LetraC6")
                End If

            Case "BEstado7"
                If contar7 = 0 Then
                    If iniciado7 = False Then
                        dsp7.Interval = New TimeSpan(0, 0, 0, 0, 9)
                        AddHandler dsp7.Tick, AddressOf intervalo7
                        dsp7.Start()
                        iniciado7 = True
                    Else
                        dsp7.Start()
                    End If
                    _FondoID7 = Rojo
                    _TextBot7 = "Conectado"
                    _FondoC7 = Gris
                    _FondoD7 = Gris
                    _LetraID7 = Blanco
                    _LetraC7 = Blanco
                    _TextAhora7 = TimeString
                    contar7 = 1

                    NotificarCambio("FondoID7")
                    NotificarCambio("TextAhora7")
                    NotificarCambio("TextBot7")
                    NotificarCambio("FondoC7")
                    NotificarCambio("FondoD7")
                    NotificarCambio("LetraC7")
                    NotificarCambio("LetraID7")

                ElseIf contar7 = 1 Then
                    _TextContador7 = Format(Horas7, "00") & ":" & Format(Minutos7, "00") & ":" & Format(Segundos7, "00") & "." & Format(MiliSegundos7, "00")
                    dsp7.Stop()
                    _TextBot7 = "Cobrando"
                    _LetraC7 = Negro
                    _LetraID7 = Negro
                    _FondoD7 = Rojo
                    _FondoID7 = Gris
                    contar7 = 2

                    NotificarCambio("FondoID7")
                    NotificarCambio("FondoD7")
                    NotificarCambio("LetraC7")
                    NotificarCambio("LetraID7")
                    NotificarCambio("TextContador7")
                    NotificarCambio("TextBot7")
                ElseIf contar7 = 2 Then
                    _FondoID7 = Verde
                    _LetraID7 = Blanco
                    _TextBot7 = "Sin conexión"
                    _FondoC7 = Nada
                    _FondoD7 = Nada
                    _LetraC7 = Blanco
                    contar7 = 0

                    iniciado7 = True
                    Horas7 = 0
                    Minutos7 = 0
                    Segundos7 = 0
                    MiliSegundos7 = 0
                    _TextContador7 = ""
                    _TextAhora7 = ""

                    NotificarCambio("FondoID7")
                    NotificarCambio("TextAhora7")
                    NotificarCambio("LetraID7")
                    NotificarCambio("TextContador7")
                    NotificarCambio("TextBot7")
                    NotificarCambio("FondoD7")
                    NotificarCambio("FondoC7")
                    NotificarCambio("LetraC7")
                End If

            Case "BEstado8"
                If contar8 = 0 Then
                    If iniciado8 = False Then
                        dsp8.Interval = New TimeSpan(0, 0, 0, 0, 9)
                        AddHandler dsp8.Tick, AddressOf intervalo8
                        dsp8.Start()
                        iniciado8 = True
                    Else
                        dsp8.Start()
                    End If
                    _FondoID8 = Rojo
                    _TextBot8 = "Conectado"
                    _FondoC8 = Gris
                    _FondoD8 = Gris
                    _LetraID8 = Blanco
                    _LetraC8 = Blanco
                    _TextAhora8 = TimeString
                    contar8 = 1

                    NotificarCambio("FondoID8")
                    NotificarCambio("TextAhora8")
                    NotificarCambio("TextBot8")
                    NotificarCambio("FondoC8")
                    NotificarCambio("FondoD8")
                    NotificarCambio("LetraC8")
                    NotificarCambio("LetraID8")

                ElseIf contar8 = 1 Then
                    _TextContador8 = Format(Horas8, "00") & ":" & Format(Minutos8, "00") & ":" & Format(Segundos8, "00") & "." & Format(MiliSegundos8, "00")
                    dsp8.Stop()
                    _TextBot8 = "Cobrando"
                    _LetraC8 = Negro
                    _LetraID8 = Negro
                    _FondoD8 = Rojo
                    _FondoID8 = Gris
                    contar8 = 2

                    NotificarCambio("FondoID8")
                    NotificarCambio("FondoD8")
                    NotificarCambio("LetraC8")
                    NotificarCambio("LetraID8")
                    NotificarCambio("TextContador8")
                    NotificarCambio("TextBot8")
                ElseIf contar8 = 2 Then
                    _FondoID8 = Verde
                    _LetraID8 = Blanco
                    _TextBot8 = "Sin conexión"
                    _FondoC8 = Nada
                    _FondoD8 = Nada
                    _LetraC8 = Blanco
                    contar8 = 0

                    iniciado8 = True
                    Horas8 = 0
                    Minutos8 = 0
                    Segundos8 = 0
                    MiliSegundos8 = 0
                    _TextContador8 = ""
                    _TextAhora8 = ""

                    NotificarCambio("FondoID8")
                    NotificarCambio("TextAhora8")
                    NotificarCambio("LetraID8")
                    NotificarCambio("TextContador8")
                    NotificarCambio("TextBot8")
                    NotificarCambio("FondoD8")
                    NotificarCambio("FondoC8")
                    NotificarCambio("LetraC8")
                End If

            Case "BEstado9"
                If contar9 = 0 Then
                    If iniciado9 = False Then
                        dsp9.Interval = New TimeSpan(0, 0, 0, 0, 9)
                        AddHandler dsp9.Tick, AddressOf intervalo9
                        dsp9.Start()
                        iniciado9 = True
                    Else
                        dsp9.Start()
                    End If
                    _FondoID9 = Rojo
                    _TextBot9 = "Conectado"
                    _FondoC9 = Gris
                    _FondoD9 = Gris
                    _LetraID9 = Blanco
                    _LetraC9 = Blanco
                    _TextAhora9 = TimeString
                    contar9 = 1

                    NotificarCambio("FondoID9")
                    NotificarCambio("TextAhora9")
                    NotificarCambio("TextBot9")
                    NotificarCambio("FondoC9")
                    NotificarCambio("FondoD9")
                    NotificarCambio("LetraC9")
                    NotificarCambio("LetraID9")

                ElseIf contar9 = 1 Then
                    _TextContador9 = Format(Horas9, "00") & ":" & Format(Minutos9, "00") & ":" & Format(Segundos9, "00") & "." & Format(MiliSegundos9, "00")
                    dsp9.Stop()
                    _TextBot9 = "Cobrando"
                    _LetraC9 = Negro
                    _LetraID9 = Negro
                    _FondoD9 = Rojo
                    _FondoID9 = Gris
                    contar9 = 2

                    NotificarCambio("FondoID9")
                    NotificarCambio("FondoD9")
                    NotificarCambio("LetraC9")
                    NotificarCambio("LetraID9")
                    NotificarCambio("TextContador9")
                    NotificarCambio("TextBot9")
                ElseIf contar9 = 2 Then
                    _FondoID9 = Verde
                    _LetraID9 = Blanco
                    _TextBot9 = "Sin conexión"
                    _FondoC9 = Nada
                    _FondoD9 = Nada
                    _LetraC9 = Blanco
                    contar9 = 0

                    iniciado9 = True
                    Horas9 = 0
                    Minutos9 = 0
                    Segundos9 = 0
                    MiliSegundos9 = 0
                    _TextContador9 = ""
                    _TextAhora9 = ""

                    NotificarCambio("FondoID9")
                    NotificarCambio("TextAhora9")
                    NotificarCambio("LetraID9")
                    NotificarCambio("TextContador9")
                    NotificarCambio("TextBot9")
                    NotificarCambio("FondoD9")
                    NotificarCambio("FondoC9")
                    NotificarCambio("LetraC9")
                End If

            Case "BEstado10"
                If contar10 = 0 Then
                    If iniciado10 = False Then
                        dsp10.Interval = New TimeSpan(0, 0, 0, 0, 10)
                        AddHandler dsp10.Tick, AddressOf intervalo10
                        dsp10.Start()
                        iniciado10 = True
                    Else
                        dsp10.Start()
                    End If
                    _FondoID10 = Rojo
                    _TextBot10 = "Conectado"
                    _FondoC10 = Gris
                    _FondoD10 = Gris
                    _LetraID10 = Blanco
                    _LetraC10 = Blanco
                    _TextAhora10 = TimeString
                    contar10 = 1

                    NotificarCambio("FondoID10")
                    NotificarCambio("TextAhora10")
                    NotificarCambio("TextBot10")
                    NotificarCambio("FondoC10")
                    NotificarCambio("FondoD10")
                    NotificarCambio("LetraC10")
                    NotificarCambio("LetraID10")

                ElseIf contar10 = 1 Then
                    _TextContador10 = Format(Horas10, "00") & ":" & Format(Minutos10, "00") & ":" & Format(Segundos10, "00") & "." & Format(MiliSegundos10, "00")
                    dsp10.Stop()
                    _TextBot10 = "Cobrando"
                    _LetraC10 = Negro
                    _LetraID10 = Negro
                    _FondoD10 = Rojo
                    _FondoID10 = Gris
                    contar10 = 2

                    NotificarCambio("FondoID10")
                    NotificarCambio("FondoD10")
                    NotificarCambio("LetraC10")
                    NotificarCambio("LetraID10")
                    NotificarCambio("TextContador10")
                    NotificarCambio("TextBot10")
                ElseIf contar10 = 2 Then
                    _FondoID10 = Verde
                    _LetraID10 = Blanco
                    _TextBot10 = "Sin conexión"
                    _FondoC10 = Nada
                    _FondoD10 = Nada
                    _LetraC10 = Blanco
                    contar10 = 0

                    iniciado10 = True
                    Horas10 = 0
                    Minutos10 = 0
                    Segundos10 = 0
                    MiliSegundos10 = 0
                    _TextContador10 = ""
                    _TextAhora10 = ""

                    NotificarCambio("FondoID10")
                    NotificarCambio("TextAhora10")
                    NotificarCambio("LetraID10")
                    NotificarCambio("TextContador10")
                    NotificarCambio("TextBot10")
                    NotificarCambio("FondoD10")
                    NotificarCambio("FondoC10")
                    NotificarCambio("LetraC10")
                End If

            Case "BEstado11"
                If contar11 = 0 Then
                    If iniciado11 = False Then
                        dsp11.Interval = New TimeSpan(0, 0, 0, 0, 11)
                        AddHandler dsp11.Tick, AddressOf intervalo11
                        dsp11.Start()
                        iniciado11 = True
                    Else
                        dsp11.Start()
                    End If
                    _FondoID11 = Rojo
                    _TextBot11 = "Conectado"
                    _FondoC11 = Gris
                    _FondoD11 = Gris
                    _LetraID11 = Blanco
                    _LetraC11 = Blanco
                    _TextAhora11 = TimeString
                    contar11 = 1

                    NotificarCambio("FondoID11")
                    NotificarCambio("TextAhora11")
                    NotificarCambio("TextBot11")
                    NotificarCambio("FondoC11")
                    NotificarCambio("FondoD11")
                    NotificarCambio("LetraC11")
                    NotificarCambio("LetraID11")

                ElseIf contar11 = 1 Then
                    _TextContador11 = Format(Horas11, "00") & ":" & Format(Minutos11, "00") & ":" & Format(Segundos11, "00") & "." & Format(MiliSegundos11, "00")
                    dsp11.Stop()
                    _TextBot11 = "Cobrando"
                    _LetraC11 = Negro
                    _LetraID11 = Negro
                    _FondoD11 = Rojo
                    _FondoID11 = Gris
                    contar11 = 2

                    NotificarCambio("FondoID11")
                    NotificarCambio("FondoD11")
                    NotificarCambio("LetraC11")
                    NotificarCambio("LetraID11")
                    NotificarCambio("TextContador11")
                    NotificarCambio("TextBot11")
                ElseIf contar11 = 2 Then
                    _FondoID11 = Verde
                    _LetraID11 = Blanco
                    _TextBot11 = "Sin conexión"
                    _FondoC11 = Nada
                    _FondoD11 = Nada
                    _LetraC11 = Blanco
                    contar11 = 0

                    iniciado11 = True
                    Horas11 = 0
                    Minutos11 = 0
                    Segundos11 = 0
                    MiliSegundos11 = 0
                    _TextContador11 = ""
                    _TextAhora11 = ""

                    NotificarCambio("FondoID11")
                    NotificarCambio("TextAhora11")
                    NotificarCambio("LetraID11")
                    NotificarCambio("TextContador11")
                    NotificarCambio("TextBot11")
                    NotificarCambio("FondoD11")
                    NotificarCambio("FondoC11")
                    NotificarCambio("LetraC11")
                End If

            Case "BEstado12"
                If contar12 = 0 Then
                    If iniciado12 = False Then
                        dsp12.Interval = New TimeSpan(0, 0, 0, 0, 12)
                        AddHandler dsp12.Tick, AddressOf intervalo12
                        dsp12.Start()
                        iniciado12 = True
                    Else
                        dsp12.Start()
                    End If
                    _FondoID12 = Rojo
                    _TextBot12 = "Conectado"
                    _FondoC12 = Gris
                    _FondoD12 = Gris
                    _LetraID12 = Blanco
                    _LetraC12 = Blanco
                    _TextAhora12 = TimeString
                    contar12 = 1

                    NotificarCambio("FondoID12")
                    NotificarCambio("TextAhora12")
                    NotificarCambio("TextBot12")
                    NotificarCambio("FondoC12")
                    NotificarCambio("FondoD12")
                    NotificarCambio("LetraC12")
                    NotificarCambio("LetraID12")

                ElseIf contar12 = 1 Then
                    _TextContador12 = Format(Horas12, "00") & ":" & Format(Minutos12, "00") & ":" & Format(Segundos12, "00") & "." & Format(MiliSegundos12, "00")
                    dsp12.Stop()
                    _TextBot12 = "Cobrando"
                    _LetraC12 = Negro
                    _LetraID12 = Negro
                    _FondoD12 = Rojo
                    _FondoID12 = Gris
                    contar12 = 2

                    NotificarCambio("FondoID12")
                    NotificarCambio("FondoD12")
                    NotificarCambio("LetraC12")
                    NotificarCambio("LetraID12")
                    NotificarCambio("TextContador12")
                    NotificarCambio("TextBot12")
                ElseIf contar12 = 2 Then
                    _FondoID12 = Verde
                    _LetraID12 = Blanco
                    _TextBot12 = "Sin conexión"
                    _FondoC12 = Nada
                    _FondoD12 = Nada
                    _LetraC12 = Blanco
                    contar12 = 0

                    iniciado12 = True
                    Horas12 = 0
                    Minutos12 = 0
                    Segundos12 = 0
                    MiliSegundos12 = 0
                    _TextContador12 = ""
                    _TextAhora12 = ""

                    NotificarCambio("FondoID12")
                    NotificarCambio("TextAhora12")
                    NotificarCambio("LetraID12")
                    NotificarCambio("TextContador12")
                    NotificarCambio("TextBot12")
                    NotificarCambio("FondoD12")
                    NotificarCambio("FondoC12")
                    NotificarCambio("LetraC12")
                End If

            Case "BEstado13"
                If contar13 = 0 Then
                    If iniciado13 = False Then
                        dsp13.Interval = New TimeSpan(0, 0, 0, 0, 13)
                        AddHandler dsp13.Tick, AddressOf intervalo13
                        dsp13.Start()
                        iniciado13 = True
                    Else
                        dsp13.Start()
                    End If
                    _FondoID13 = Rojo
                    _TextBot13 = "Conectado"
                    _FondoC13 = Gris
                    _FondoD13 = Gris
                    _LetraID13 = Blanco
                    _LetraC13 = Blanco
                    _TextAhora13 = TimeString
                    contar13 = 1

                    NotificarCambio("FondoID13")
                    NotificarCambio("TextAhora13")
                    NotificarCambio("TextBot13")
                    NotificarCambio("FondoC13")
                    NotificarCambio("FondoD13")
                    NotificarCambio("LetraC13")
                    NotificarCambio("LetraID13")

                ElseIf contar13 = 1 Then
                    _TextContador13 = Format(Horas13, "00") & ":" & Format(Minutos13, "00") & ":" & Format(Segundos13, "00") & "." & Format(MiliSegundos13, "00")
                    dsp13.Stop()
                    _TextBot13 = "Cobrando"
                    _LetraC13 = Negro
                    _LetraID13 = Negro
                    _FondoD13 = Rojo
                    _FondoID13 = Gris
                    contar13 = 2

                    NotificarCambio("FondoID13")
                    NotificarCambio("FondoD13")
                    NotificarCambio("LetraC13")
                    NotificarCambio("LetraID13")
                    NotificarCambio("TextContador13")
                    NotificarCambio("TextBot13")
                ElseIf contar13 = 2 Then
                    _FondoID13 = Verde
                    _LetraID13 = Blanco
                    _TextBot13 = "Sin conexión"
                    _FondoC13 = Nada
                    _FondoD13 = Nada
                    _LetraC13 = Blanco
                    contar13 = 0

                    iniciado13 = True
                    Horas13 = 0
                    Minutos13 = 0
                    Segundos13 = 0
                    MiliSegundos13 = 0
                    _TextContador13 = ""
                    _TextAhora13 = ""

                    NotificarCambio("FondoID13")
                    NotificarCambio("TextAhora13")
                    NotificarCambio("LetraID13")
                    NotificarCambio("TextContador13")
                    NotificarCambio("TextBot13")
                    NotificarCambio("FondoD13")
                    NotificarCambio("FondoC13")
                    NotificarCambio("LetraC13")
                End If

            Case "BEstado14"
                If contar14 = 0 Then
                    If iniciado14 = False Then
                        dsp14.Interval = New TimeSpan(0, 0, 0, 0, 14)
                        AddHandler dsp14.Tick, AddressOf intervalo14
                        dsp14.Start()
                        iniciado14 = True
                    Else
                        dsp14.Start()
                    End If
                    _FondoID14 = Rojo
                    _TextBot14 = "Conectado"
                    _FondoC14 = Gris
                    _FondoD14 = Gris
                    _LetraID14 = Blanco
                    _LetraC14 = Blanco
                    _TextAhora14 = TimeString
                    contar14 = 1

                    NotificarCambio("FondoID14")
                    NotificarCambio("TextAhora14")
                    NotificarCambio("TextBot14")
                    NotificarCambio("FondoC14")
                    NotificarCambio("FondoD14")
                    NotificarCambio("LetraC14")
                    NotificarCambio("LetraID14")

                ElseIf contar14 = 1 Then
                    _TextContador14 = Format(Horas14, "00") & ":" & Format(Minutos14, "00") & ":" & Format(Segundos14, "00") & "." & Format(MiliSegundos14, "00")
                    dsp14.Stop()
                    _TextBot14 = "Cobrando"
                    _LetraC14 = Negro
                    _LetraID14 = Negro
                    _FondoD14 = Rojo
                    _FondoID14 = Gris
                    contar14 = 2

                    NotificarCambio("FondoID14")
                    NotificarCambio("FondoD14")
                    NotificarCambio("LetraC14")
                    NotificarCambio("LetraID14")
                    NotificarCambio("TextContador14")
                    NotificarCambio("TextBot14")
                ElseIf contar14 = 2 Then
                    _FondoID14 = Verde
                    _LetraID14 = Blanco
                    _TextBot14 = "Sin conexión"
                    _FondoC14 = Nada
                    _FondoD14 = Nada
                    _LetraC14 = Blanco
                    contar14 = 0

                    iniciado14 = True
                    Horas14 = 0
                    Minutos14 = 0
                    Segundos14 = 0
                    MiliSegundos14 = 0
                    _TextContador14 = ""
                    _TextAhora14 = ""

                    NotificarCambio("FondoID14")
                    NotificarCambio("TextAhora14")
                    NotificarCambio("LetraID14")
                    NotificarCambio("TextContador14")
                    NotificarCambio("TextBot14")
                    NotificarCambio("FondoD14")
                    NotificarCambio("FondoC14")
                    NotificarCambio("LetraC14")
                End If


            Case "AdminUsuarios"
                Dim _AdminUsuarios As New AdminUsuarios
                _AdminUsuarios.Show()


            Case "Inventario"
                Dim _Inventario As New Inventario
                _Inventario.Show()

            Case "Moneda"
                Dim _EDi As New EspecificacionDinero
                _EDi.Show()

            Case "IVA"
                Dim _IVA As New IVA
                _IVA.Show()

            Case "Tarifas"
                Dim _Tari As New Tarifas
                _Tari.Show()

            Case "DescuentosSegunCodigo"
                Dim _DSC As New DescuentosSegunCodigo
                _DSC.Show()

            Case "Caja"
                Dim _Caja As New Caja
                _Caja.Show()

            Case "Socios"
                Dim _Socios As New Socios
                _Socios.Show()

            Case "Cupones"
                Dim _Cupones As New Cupones
                _Cupones.Show()


        End Select
    End Sub

    '--------------------------------------CONTADORES/INTERVALOS

    Public Function intervalo()
        MiliSegundos1 += 1
        If Me.MiliSegundos1 = 60 Then
            MiliSegundos1 = 0
            Segundos1 += 1
        End If
        If Segundos1 = 60 Then
            Minutos1 += 1
            Segundos1 = 0
        End If
        If Minutos1 = 60 Then
            Minutos1 = 0
            Horas1 += 1
        End If
        _TextContador1 = Format(Horas1, "00") & ":" & Format(Minutos1, "00") & ":" & Format(Segundos1, "00") & "." & Format(MiliSegundos1, "00")
        NotificarCambio("TextContador1")
        Return _TextContador1
    End Function

    Public Function intervalo2()
        MiliSegundos2 += 1
        If Me.MiliSegundos2 = 60 Then
            MiliSegundos2 = 0
            Segundos2 += 1
        End If
        If Segundos2 = 60 Then
            Minutos2 += 1
            Segundos2 = 0
        End If
        If Minutos2 = 60 Then
            Minutos2 = 0
            Horas2 += 1
        End If
        _TextContador2 = Format(Horas2, "00") & ":" & Format(Minutos2, "00") & ":" & Format(Segundos2, "00") & "." & Format(MiliSegundos2, "00")
        NotificarCambio("TextContador2")
        Return _TextContador2
    End Function

    Public Function intervalo3()
        MiliSegundos3 += 1
        If Me.MiliSegundos3 = 60 Then
            MiliSegundos3 = 0
            Segundos3 += 1
        End If
        If Segundos3 = 60 Then
            Minutos3 += 1
            Segundos3 = 0
        End If
        If Minutos3 = 60 Then
            Minutos3 = 0
            Horas3 += 1
        End If
        _TextContador3 = Format(Horas3, "00") & ":" & Format(Minutos3, "00") & ":" & Format(Segundos3, "00") & "." & Format(MiliSegundos3, "00")
        NotificarCambio("TextContador3")
        Return _TextContador3
    End Function

    Public Function intervalo4()
        MiliSegundos4 += 1
        If Me.MiliSegundos4 = 60 Then
            MiliSegundos4 = 0
            Segundos4 += 1
        End If
        If Segundos4 = 60 Then
            Minutos4 += 1
            Segundos4 = 0
        End If
        If Minutos4 = 60 Then
            Minutos4 = 0
            Horas4 += 1
        End If
        _TextContador4 = Format(Horas4, "00") & ":" & Format(Minutos4, "00") & ":" & Format(Segundos4, "00") & "." & Format(MiliSegundos4, "00")
        NotificarCambio("TextContador4")
        Return _TextContador4
    End Function

    Public Function intervalo5()
        MiliSegundos5 += 1
        If Me.MiliSegundos5 = 60 Then
            MiliSegundos5 = 0
            Segundos5 += 1
        End If
        If Segundos5 = 60 Then
            Minutos5 += 1
            Segundos5 = 0
        End If
        If Minutos5 = 60 Then
            Minutos5 = 0
            Horas5 += 1
        End If
        _TextContador5 = Format(Horas5, "00") & ":" & Format(Minutos5, "00") & ":" & Format(Segundos5, "00") & "." & Format(MiliSegundos5, "00")
        NotificarCambio("TextContador5")
        Return _TextContador5
    End Function

    Public Function intervalo6()
        MiliSegundos6 += 1
        If Me.MiliSegundos6 = 60 Then
            MiliSegundos6 = 0
            Segundos6 += 1
        End If
        If Segundos6 = 60 Then
            Minutos6 += 1
            Segundos6 = 0
        End If
        If Minutos6 = 60 Then
            Minutos6 = 0
            Horas6 += 1
        End If
        _TextContador6 = Format(Horas6, "00") & ":" & Format(Minutos6, "00") & ":" & Format(Segundos6, "00") & "." & Format(MiliSegundos6, "00")
        NotificarCambio("TextContador6")
        Return _TextContador6
    End Function

    Public Function intervalo7()
        MiliSegundos7 += 1
        If Me.MiliSegundos7 = 60 Then
            MiliSegundos7 = 0
            Segundos7 += 1
        End If
        If Segundos7 = 60 Then
            Minutos7 += 1
            Segundos7 = 0
        End If
        If Minutos7 = 60 Then
            Minutos7 = 0
            Horas7 += 1
        End If
        _TextContador7 = Format(Horas7, "00") & ":" & Format(Minutos7, "00") & ":" & Format(Segundos7, "00") & "." & Format(MiliSegundos7, "00")
        NotificarCambio("TextContador7")
        Return _TextContador7
    End Function

    Public Function intervalo8()
        MiliSegundos8 += 1
        If Me.MiliSegundos8 = 60 Then
            MiliSegundos8 = 0
            Segundos8 += 1
        End If
        If Segundos8 = 60 Then
            Minutos8 += 1
            Segundos8 = 0
        End If
        If Minutos8 = 60 Then
            Minutos8 = 0
            Horas8 += 1
        End If
        _TextContador8 = Format(Horas8, "00") & ":" & Format(Minutos8, "00") & ":" & Format(Segundos8, "00") & "." & Format(MiliSegundos8, "00")
        NotificarCambio("TextContador8")
        Return _TextContador8
    End Function

    Public Function intervalo9()
        MiliSegundos9 += 1
        If Me.MiliSegundos9 = 60 Then
            MiliSegundos9 = 0
            Segundos9 += 1
        End If
        If Segundos9 = 60 Then
            Minutos9 += 1
            Segundos9 = 0
        End If
        If Minutos9 = 60 Then
            Minutos9 = 0
            Horas9 += 1
        End If
        _TextContador9 = Format(Horas9, "00") & ":" & Format(Minutos9, "00") & ":" & Format(Segundos9, "00") & "." & Format(MiliSegundos9, "00")
        NotificarCambio("TextContador9")
        Return _TextContador9
    End Function

    Public Function intervalo10()
        MiliSegundos10 += 1
        If Me.MiliSegundos10 = 60 Then
            MiliSegundos10 = 0
            Segundos10 += 1
        End If
        If Segundos10 = 60 Then
            Minutos10 += 1
            Segundos10 = 0
        End If
        If Minutos10 = 60 Then
            Minutos10 = 0
            Horas10 += 1
        End If
        _TextContador10 = Format(Horas10, "00") & ":" & Format(Minutos10, "00") & ":" & Format(Segundos10, "00") & "." & Format(MiliSegundos10, "00")
        NotificarCambio("TextContador10")
        Return _TextContador10
    End Function

    Public Function intervalo11()
        MiliSegundos11 += 1
        If Me.MiliSegundos11 = 60 Then
            MiliSegundos11 = 0
            Segundos11 += 1
        End If
        If Segundos11 = 60 Then
            Minutos11 += 1
            Segundos11 = 0
        End If
        If Minutos11 = 60 Then
            Minutos11 = 0
            Horas11 += 1
        End If
        _TextContador11 = Format(Horas11, "00") & ":" & Format(Minutos11, "00") & ":" & Format(Segundos11, "00") & "." & Format(MiliSegundos11, "00")
        NotificarCambio("TextContador11")
        Return _TextContador11
    End Function

    Public Function intervalo12()
        MiliSegundos12 += 1
        If Me.MiliSegundos12 = 60 Then
            MiliSegundos12 = 0
            Segundos12 += 1
        End If
        If Segundos12 = 60 Then
            Minutos12 += 1
            Segundos12 = 0
        End If
        If Minutos12 = 60 Then
            Minutos12 = 0
            Horas12 += 1
        End If
        _TextContador12 = Format(Horas12, "00") & ":" & Format(Minutos12, "00") & ":" & Format(Segundos12, "00") & "." & Format(MiliSegundos12, "00")
        NotificarCambio("TextContador12")
        Return _TextContador12
    End Function

    Public Function intervalo13()
        MiliSegundos13 += 1
        If Me.MiliSegundos13 = 60 Then
            MiliSegundos13 = 0
            Segundos13 += 1
        End If
        If Segundos13 = 60 Then
            Minutos13 += 1
            Segundos13 = 0
        End If
        If Minutos13 = 60 Then
            Minutos13 = 0
            Horas13 += 1
        End If
        _TextContador13 = Format(Horas13, "00") & ":" & Format(Minutos13, "00") & ":" & Format(Segundos13, "00") & "." & Format(MiliSegundos13, "00")
        NotificarCambio("TextContador13")
        Return _TextContador13
    End Function

    Public Function intervalo14()
        MiliSegundos14 += 1
        If Me.MiliSegundos14 = 60 Then
            MiliSegundos14 = 0
            Segundos14 += 1
        End If
        If Segundos14 = 60 Then
            Minutos14 += 1
            Segundos14 = 0
        End If
        If Minutos14 = 60 Then
            Minutos14 = 0
            Horas14 += 1
        End If
        _TextContador14 = Format(Horas14, "00") & ":" & Format(Minutos14, "00") & ":" & Format(Segundos14, "00") & "." & Format(MiliSegundos14, "00")
        NotificarCambio("TextContador14")
        Return _TextContador14
    End Function

End Class