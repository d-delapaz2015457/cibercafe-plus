﻿Imports System.ComponentModel

Public Class ModeloVistaCrearCupon
    Implements INotifyPropertyChanged, ICommand

    Private _Instancia As ModeloVistaCrearCupon
    Private _CSo As CrearCupon
    Private _TextName As String
    Private _TextClave As String
    Private _TextConfiClave As String
    Private _TextSaldo As Decimal

    Public Sub New(ByRef _instance As CrearCupon)
        Me._CSo = _instance
        Me.Instancia = Me
    End Sub

    '-----TODAS LAS PROPIEDADES ---------
    Public Property Instancia As ModeloVistaCrearCupon
        Get
            Return _Instancia
        End Get
        Set(value As ModeloVistaCrearCupon)
            _Instancia = value
            NotificarCambio("Instancia")
        End Set
    End Property

    Public Property TextName As String
        Get
            Return _TextName
        End Get
        Set(value As String)
            _TextName = value
            NotificarCambio("TextName")
        End Set
    End Property

    Public Property TextClave As String
        Get
            Return _TextClave
        End Get
        Set(value As String)
            _TextClave = value
            NotificarCambio("TextClave")
        End Set
    End Property

    Public Property TextConfiClave As String
        Get
            Return _TextConfiClave
        End Get
        Set(value As String)
            _TextConfiClave = value
            NotificarCambio("TextConfiClave")
        End Set
    End Property

    Public Property TextSaldo As Decimal
        Get
            Return _TextSaldo
        End Get
        Set(value As Decimal)
            _TextSaldo = value
            NotificarCambio("TextSaldo")
        End Set
    End Property

    '----------- MÉTODOS
    Public Sub NotificarCambio(ByVal Propiedad As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(Propiedad))
    End Sub

    Public Event CanExecuteChanged As EventHandler Implements ICommand.CanExecuteChanged
    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

    Public Function CanExecute(parameter As Object) As Boolean Implements ICommand.CanExecute
        Return True
    End Function

    Public Sub Execute(Parametro As Object) Implements ICommand.Execute
        Select Case Parametro
            Case "Ok"
                Using context As New CiberCafePlusEntities1

                    Dim correct As Integer = 0

                    If IsNumeric(_TextSaldo) Then
                    Else
                        correct = 1
                    End If
                    If _TextSaldo < 10 Then
                        correct = 1
                        MsgBox("El saldo inicial debe ser de Q10.00 en adelante")
                    End If

                    If _TextClave <> _TextConfiClave Then
                        correct = 1
                        MsgBox("Error al verificar la clave")
                    End If

                    If IsNothing(_TextName) Or IsNothing(_TextClave) Or IsNothing(_TextConfiClave) Or IsNothing(_TextSaldo) Then
                        correct = 1
                        MsgBox("No se llenaron todos los campos")
                    End If

                    If correct = 0 Then
                        context.SP_AgreCupones(_TextName, _TextClave, _TextSaldo, True)
                        _CSo.Close()
                        Dim _Admin As New Cupones
                        _Admin.Show()
                    Else
                    End If

                End Using
            Case "Cancelar"
                MsgBox("Saldrá del registro")
                _CSo.Close()
                Dim _Admin As New Cupones
                _Admin.Show()
            Case "CerrarVentana"
                _CSo.Close()

        End Select
    End Sub
End Class
