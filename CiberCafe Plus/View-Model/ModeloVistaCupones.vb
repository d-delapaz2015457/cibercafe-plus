﻿Imports System.ComponentModel

Public Class ModeloVistaCupones
    Implements INotifyPropertyChanged, ICommand
    Public _Instancia As ModeloVistaCupones
    Private _Cupones As Cupones
    Private _TablaCupones As New Collection
    Private _SelecCupon As New VIEW_Cupones

    Public Sub New(ByRef _instance As Cupones)
        Me._Cupones = _instance
        Me.Instancia = Me
        TablaCupones = LlenarTabla()
        NotificarCambio("TablaCupones")
    End Sub

    '-----TODAS LAS PROPIEDADES ---------
    Public Property Instancia As ModeloVistaCupones
        Get
            Return _Instancia
        End Get
        Set(value As ModeloVistaCupones)
            _Instancia = value
            NotificarCambio("Instancia")
        End Set
    End Property

    Public Property TablaCupones As Collection
        Get
            Return _TablaCupones
        End Get
        Set(value As Collection)
            _TablaCupones = value
            NotificarCambio("TablaCupones")
        End Set
    End Property

    Public Property SelecCupon As VIEW_Cupones
        Get
            Return _SelecCupon
        End Get
        Set(value As VIEW_Cupones)
            _SelecCupon = value
            NotificarCambio("SelecCupon")
        End Set
    End Property

    '----------- MÉTODOS
    Public Sub NotificarCambio(ByVal Propiedad As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(Propiedad))
    End Sub

    Public Event CanExecuteChanged As EventHandler Implements ICommand.CanExecuteChanged
    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

    Public Function CanExecute(parameter As Object) As Boolean Implements ICommand.CanExecute
        Return True
    End Function

    Public Sub Execute(Parametro As Object) Implements ICommand.Execute
        Select Case Parametro
            Case "Nuevo"
                Dim _Nuevo As New CrearCupon
                _Nuevo.Show()
                _Cupones.Close()
            Case "Ok"
                _Cupones.Close()
            Case "Modificar"
                Dim _Modi As New ModificarCupon
                ModeloVistaModificarCupon.Instancia.ObtenerId(SelecCupon.ID)
                _Modi.Show()
                _Cupones.Close()
            Case "Eliminar"
                Using context As New CiberCafePlusEntities1
                    If SelecCupon.ID > 0 Then
                        Dim sociosQuery = From x In context.VIEW_Cupones Select x
                        context.SP_EliCupones(SelecCupon.ID)
                        TablaCupones = LlenarTabla()
                    End If
                End Using
            Case "CerrarVentana"
                _Cupones.Close()
        End Select
    End Sub

    Public Function LlenarTabla() As Collection
        Using context As New CiberCafePlusEntities1
            TablaCupones.Clear()
            Dim sociosQuery = From x In context.VIEW_Cupones Select x
            For Each _Consulta In sociosQuery
                TablaCupones.Add(_Consulta)
            Next
            NotificarCambio("TablaCupones")
        End Using
        Return TablaCupones
    End Function

End Class