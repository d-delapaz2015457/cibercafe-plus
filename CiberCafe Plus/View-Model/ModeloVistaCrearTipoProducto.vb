﻿Imports System.ComponentModel

Public Class ModeloVistaCrearTipoProducto
    Implements INotifyPropertyChanged, ICommand
    Private _Instancia As ModeloVistaCrearTipoProducto
    Private _CrearTipoProducto As CrearTipoProducto

    Public Sub New(ByRef _instance As CrearTipoProducto)
        Me._CrearTipoProducto = _instance
        Me.Instancia = Me
    End Sub

    '-----TODAS LAS PROPIEDADES ---------
    Public Property Instancia As ModeloVistaCrearTipoProducto
        Get
            Return _Instancia
        End Get
        Set(value As ModeloVistaCrearTipoProducto)
            _Instancia = value
            NotificarCambio("Instancia")
        End Set
    End Property


    '----------- MÉTODOS
    Public Sub NotificarCambio(ByVal Propiedad As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(Propiedad))
    End Sub

    Public Event CanExecuteChanged As EventHandler Implements ICommand.CanExecuteChanged
    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

    Public Function CanExecute(parameter As Object) As Boolean Implements ICommand.CanExecute
        Return True
    End Function

    Public Sub Execute(Parametro As Object) Implements ICommand.Execute
        Select Case Parametro

            Case "Ok"
                _CrearTipoProducto.Close()

            Case "CerrarVentana"
                _CrearTipoProducto.Close()
        End Select
    End Sub
End Class
