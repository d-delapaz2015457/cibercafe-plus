﻿Imports System.ComponentModel

Public Class ModeloVistaIVA
    Implements INotifyPropertyChanged, ICommand
    Private _Instancia As ModeloVistaIVA
    Private _IVA As IVA

    Public Sub New(ByRef _instance As IVA)
        Me._IVA = _instance
        Me.Instancia = Me
    End Sub

    '-----TODAS LAS PROPIEDADES ---------
    Public Property Instancia As ModeloVistaIVA
        Get
            Return _Instancia
        End Get
        Set(value As ModeloVistaIVA)
            _Instancia = value
            NotificarCambio("Instancia")
        End Set
    End Property


    '----------- MÉTODOS
    Public Sub NotificarCambio(ByVal Propiedad As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(Propiedad))
    End Sub

    Public Event CanExecuteChanged As EventHandler Implements ICommand.CanExecuteChanged
    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

    Public Function CanExecute(parameter As Object) As Boolean Implements ICommand.CanExecute
        Return True
    End Function

    Public Sub Execute(Parametro As Object) Implements ICommand.Execute
        Select Case Parametro

            Case "Ok"
                _IVA.Close()

            Case "CerrarVentana"
                _IVA.Close()
        End Select
    End Sub
End Class
