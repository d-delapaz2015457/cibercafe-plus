﻿Imports System.ComponentModel

Public Class ModeloVistaCaja
    Implements INotifyPropertyChanged, ICommand
    Public _Instancia As ModeloVistaCaja
    Private _Caja As Caja
    Private _TablaSocio As New Collection
    Private _SelecSocio As New VIEW_Socios

    Public Sub New(ByRef _instance As Caja)
        Me._Caja = _instance
        Me.Instancia = Me
        TablaSocio = LlenarTabla()
        NotificarCambio("TablaSocio")
    End Sub

    '-----TODAS LAS PROPIEDADES ---------
    Public Property Instancia As ModeloVistaCaja
        Get
            Return _Instancia
        End Get
        Set(value As ModeloVistaCaja)
            _Instancia = value
            NotificarCambio("Instancia")
        End Set
    End Property

    Public Property TablaSocio As Collection
        Get
            Return _TablaSocio
        End Get
        Set(value As Collection)
            _TablaSocio = value
            NotificarCambio("TablaSocio")
        End Set
    End Property

    Public Property SelecSocio As VIEW_Socios
        Get
            Return _SelecSocio
        End Get
        Set(value As VIEW_Socios)
            _SelecSocio = value
            NotificarCambio("SelecSocio")
        End Set
    End Property

    '----------- MÉTODOS
    Public Sub NotificarCambio(ByVal Propiedad As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(Propiedad))
    End Sub

    Public Event CanExecuteChanged As EventHandler Implements ICommand.CanExecuteChanged
    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

    Public Function CanExecute(parameter As Object) As Boolean Implements ICommand.CanExecute
        Return True
    End Function

    Public Sub Execute(Parametro As Object) Implements ICommand.Execute
        Select Case Parametro
            Case "Nuevo"
                Dim _Nuevo As New CrearSocio
                _Nuevo.Show()
                _Caja.Close()
            Case "Ok"
                _Caja.Close()
            Case "Modificar"
                Dim _Modi As New ModificarSocio
                ModeloVistaModificarSocio.Instancia.ObtenerId(SelecSocio.ID)
                _Modi.Show()
                _Caja.Close()
            Case "Eliminar"
                Using context As New CiberCafePlusEntities1
                    If SelecSocio.ID > 0 Then
                        Dim sociosQuery = From x In context.VIEW_Socios Select x
                        context.SP_EliSocios(SelecSocio.ID)
                        TablaSocio = LlenarTabla()
                    End If
                End Using
            Case "CerrarVentana"
                _Caja.Close()
        End Select
    End Sub

    Public Function LlenarTabla() As Collection
        Using context As New CiberCafePlusEntities1
            TablaSocio.Clear()
            Dim sociosQuery = From x In context.VIEW_Socios Select x
            For Each _Consulta In sociosQuery
                TablaSocio.Add(_Consulta)
            Next
            NotificarCambio("TablaSocio")
        End Using
        Return TablaSocio
    End Function

End Class
