﻿Imports System.ComponentModel

Public Class ModeloVistaAdminUsuarios
    Implements INotifyPropertyChanged, ICommand
    Private _Instancia As ModeloVistaAdminUsuarios
    Private _AUs As AdminUsuarios

    Public Sub New(ByRef _instance As AdminUsuarios)
        Me._AUs = _instance
        Me.Instancia = Me
    End Sub

    '-----TODAS LAS PROPIEDADES ---------
    Public Property Instancia As ModeloVistaAdminUsuarios
        Get
            Return _Instancia
        End Get
        Set(value As ModeloVistaAdminUsuarios)
            _Instancia = value
            NotificarCambio("Instancia")
        End Set
    End Property


    '----------- MÉTODOS--------'
    Public Sub NotificarCambio(ByVal Propiedad As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(Propiedad))
    End Sub

    Public Event CanExecuteChanged As EventHandler Implements ICommand.CanExecuteChanged
    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

    Public Function CanExecute(parameter As Object) As Boolean Implements ICommand.CanExecute
        Return True
    End Function

    Public Sub Execute(Parametro As Object) Implements ICommand.Execute
        Select Case Parametro
            Case "Nuevo"
                Dim _Nuevo As New CrearSocio
                _Nuevo.Show()
            Case "Modificar"

            Case "Eliminar"

            Case "Ok"
                _AUs.Close()

            Case "CerrarVentana"
                _AUs.Close()
        End Select
    End Sub
End Class
