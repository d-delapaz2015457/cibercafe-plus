﻿Imports System.ComponentModel

Public Class ModeloVistaModificarSocio
    Implements INotifyPropertyChanged, ICommand
    Private _CSo As ModificarSocio
    Private _TextName As String
    Private _TextClave As String
    Private _TextConfiClave As String
    Private _TextNombres As String
    Private _TextApellidos As String
    Private _TextDPI As Integer
    Private _TextTipo As String
    Private _TextNaci As String
    Private _TextCel As Integer
    Private _TextDirec As String
    Private _TextSaldo As Decimal
    Private _IdSocio As Integer
    'Private _Instancia As ModeloVistaModificarSocio

    Private Shared _Instancia As ModeloVistaModificarSocio = Nothing
    Private Shared ReadOnly _Sync As New Object

    Public Shared ReadOnly Property Instancia() As ModeloVistaModificarSocio
        Get
            If _Instancia Is Nothing Then
                SyncLock _Sync
                    If _Instancia Is Nothing Then
                        _Instancia = New ModeloVistaModificarSocio()
                    End If
                End SyncLock
            End If
            Return _Instancia
        End Get
    End Property

    Public Sub Actu(ByRef _instance As ModificarSocio)
        Me._CSo = _instance
    End Sub

    Public Property TextName As String
        Get
            Return _TextName
        End Get
        Set(value As String)
            _TextName = value
            NotificarCambio("TextName")
        End Set
    End Property

    Public Property TextClave As String
        Get
            Return _TextClave
        End Get
        Set(value As String)
            _TextClave = value
            NotificarCambio("TextClave")
        End Set
    End Property

    Public Property TextConfiClave As String
        Get
            Return _TextConfiClave
        End Get
        Set(value As String)
            _TextConfiClave = value
            NotificarCambio("TextConfiClave")
        End Set
    End Property

    Public Property TextNombres As String
        Get
            Return _TextNombres
        End Get
        Set(value As String)
            _TextNombres = value
            NotificarCambio("TextNombres")
        End Set
    End Property

    Public Property TextApellidos As String
        Get
            Return _TextApellidos
        End Get
        Set(value As String)
            _TextApellidos = value
            NotificarCambio("TextApellidos")
        End Set
    End Property

    Public Property TextDPI As Integer
        Get
            Return _TextDPI
        End Get
        Set(value As Integer)
            _TextDPI = value
            NotificarCambio("TextDPI")
        End Set
    End Property

    Public Property TextTipo As String
        Get
            Return _TextTipo
        End Get
        Set(value As String)
            _TextTipo = value
            NotificarCambio("TextTipo")
        End Set
    End Property

    Public Property TextNaci As String
        Get
            Return _TextNaci
        End Get
        Set(value As String)
            _TextNaci = value
            NotificarCambio("TextNaci")
        End Set
    End Property

    Public Property TextCel As Integer
        Get
            Return _TextCel
        End Get
        Set(value As Integer)
            _TextCel = value
            NotificarCambio("TextCel")
        End Set
    End Property

    Public Property TextDirec As String
        Get
            Return _TextDirec
        End Get
        Set(value As String)
            _TextDirec = value
            NotificarCambio("TextDirec")
        End Set
    End Property

    Public Property TextSaldo As Decimal
        Get
            Return _TextSaldo
        End Get
        Set(value As Decimal)
            _TextSaldo = value
            NotificarCambio("TextSaldo")
        End Set
    End Property

    '----------- MÉTODOS
    Public Sub NotificarCambio(ByVal Propiedad As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(Propiedad))
    End Sub

    Public Event CanExecuteChanged As EventHandler Implements ICommand.CanExecuteChanged
    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

    Public Function CanExecute(parameter As Object) As Boolean Implements ICommand.CanExecute
        Return True
    End Function

    Public Sub Execute(Parametro As Object) Implements ICommand.Execute
        Select Case Parametro
            Case "Ok"
                Using context As New CiberCafePlusEntities1

                    Dim correct As Integer = 0
                    Dim sociosQuery = From x In context.Socios Select x
                    Dim prubaTipo As Integer = 0

                    For Each socio In sociosQuery
                        If socio.name.Equals(_TextName) Then
                            correct = 1
                            MsgBox("Ya existe un socio con ese nombre")
                            Exit For
                        End If
                    Next
                    If _TextCel = 0 Then
                        correct = 1
                        MsgBox("En 'Teléfono', solo se aceptan valores numéricos mayores a 0")
                    End If
                    If _TextDPI = 0 Then
                        correct = 1
                        MsgBox("En 'DPI', solo se aceptan valores numéricos mayores a 0")
                    End If
                    If _TextSaldo < 100 Then
                        correct = 1
                        MsgBox("El saldo inicial debe ser de 100 en adelante")
                    End If
                    If _TextClave <> _TextConfiClave Then
                        correct = 1
                        MsgBox("Error al verificar la clave")
                    End If
                    Select Case _TextTipo
                        Case "Oro"
                        Case "Plata"
                        Case "Bronce"
                        Case Else
                            correct = 1
                            MsgBox("El tipo de socio indicado no existe")
                    End Select
                    If IsDate(_TextNaci) Then
                    Else
                        correct = 1
                        MsgBox("Formato de fecha de nacimiento: dd/mm/aaaa")
                    End If
                    If IsNothing(_TextNombres) Or IsNothing(_TextApellidos) Or IsNothing(_TextName) Or IsNothing(_TextClave) Or
                        IsNothing(_TextConfiClave) Or IsNothing(_TextCel) Or IsNothing(_TextDirec) Or IsNothing(_TextSaldo) Or IsNothing(_TextCel) Or
                        IsNothing(_TextDPI) Or IsNothing(_TextNaci) Or IsNothing(_TextTipo) Then
                        correct = 1
                        MsgBox("No se llenaron todos los campos")
                    End If

                    If correct = 0 Then
                        context.SP_ActuSocios(_TextName, _TextClave, _TextNombres, _TextApellidos, _TextDPI, _TextTipo, _TextNaci, _TextCel, _TextDirec, _TextSaldo, True, _IdSocio)
                        _CSo.Close()
                        Dim _Admin As New Socios
                        _Admin.Show()
                    Else

                    End If

                End Using
            Case "Cancelar"
                MsgBox("Saldrá del registro")
                _CSo.Close()
                Dim _Admin As New Socios
                _Admin.Show()

            Case "CerrarVentana"
                _CSo.Close()
        End Select
    End Sub

    Public Sub ObtenerId(id As Integer)
        _IdSocio = id
    End Sub
End Class