﻿Imports System.ComponentModel

Public Class ModeloVistaDescuentosSegunCodigo
    Implements INotifyPropertyChanged, ICommand
    Private _Instancia As ModeloVistaDescuentosSegunCodigo
    Private _DSC As DescuentosSegunCodigo
    Private _ValorMoneda As String


    Public Sub New(ByRef _instance As DescuentosSegunCodigo)
        Me._DSC = _instance
        Me.Instancia = Me

        Using context As New CiberCafePlusEntities1
            Dim adminQuery = From x In context.Monedas Select x
            Dim prueba As Integer = 0
            For Each moneda In adminQuery
                If moneda.name.Equals("Quetzales") And moneda.estado = True Then
                    prueba = 1
                    _ValorMoneda = "Q"
                    NotificarCambio("ValorMoneda")
                    Exit For
                End If
            Next
            If prueba = 0 Then
                _ValorMoneda = "$"
                NotificarCambio("ValorMoneda")
            End If
        End Using

    End Sub

    '-----TODAS LAS PROPIEDADES ---------
    Public Property Instancia As ModeloVistaDescuentosSegunCodigo
        Get
            Return _Instancia
        End Get
        Set(value As ModeloVistaDescuentosSegunCodigo)
            _Instancia = value
            NotificarCambio("Instancia")
        End Set
    End Property

    Public Property ValorMoneda As String
        Get
            Return _ValorMoneda
        End Get
        Set(value As String)
            _ValorMoneda = value
            NotificarCambio("_ValorMoneda")
        End Set
    End Property

    '----------- MÉTODOS
    Public Sub NotificarCambio(ByVal Propiedad As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(Propiedad))
    End Sub

    Public Event CanExecuteChanged As EventHandler Implements ICommand.CanExecuteChanged
    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

    Public Function CanExecute(parameter As Object) As Boolean Implements ICommand.CanExecute
        Return True
    End Function

    Public Sub Execute(Parametro As Object) Implements ICommand.Execute
        Select Case Parametro

            Case "Ok"
                _DSC.Close()

            Case "CerrarVentana"
                _DSC.Close()
        End Select
    End Sub
End Class
