﻿Imports System.ComponentModel

Public Class ModeloVistaCobrar
    Implements INotifyPropertyChanged, ICommand
    Private _Cobrar As Cobrar
    Private _TablaCobrar As New Collection

    '--------------------Singleton
    Private Shared _Instancia As ModeloVistaCobrar = Nothing
    Private Shared ReadOnly _Sync As New Object

    Private Sub New()
    End Sub

    Public Shared ReadOnly Property Instancia() As ModeloVistaCobrar
        Get
            If _Instancia Is Nothing Then
                SyncLock _Sync
                    If _Instancia Is Nothing Then
                        _Instancia = New ModeloVistaCobrar()
                    End If
                End SyncLock
            End If
            Return _Instancia
        End Get
    End Property

    Public Sub Actu(ByRef _instance As Cobrar)
        Me._Cobrar = _instance
    End Sub

    '--------------------------------------------

    Public Property TablaCobrar As Collection
        Get
            Return _TablaCobrar
        End Get
        Set(value As Collection)
            _TablaCobrar = value
            NotificarCambio("TablaCobrar")
        End Set
    End Property


    '----------- MÉTODOS
    Public Sub NotificarCambio(ByVal Propiedad As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(Propiedad))
    End Sub

    Public Event CanExecuteChanged As EventHandler Implements ICommand.CanExecuteChanged
    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

    Public Function CanExecute(parameter As Object) As Boolean Implements ICommand.CanExecute
        Return True
    End Function

    Public Sub Execute(Parametro As Object) Implements ICommand.Execute
        Select Case Parametro

            Case "Ok"
                ModeloVistaPrincipal.Instancia().Limpiar1()
                _Cobrar.Close()

            Case "Cancelar"
                ModeloVistaPrincipal.Instancia().Seguir1()
                _Cobrar.Close()

            Case "CerrarVentana"
                ModeloVistaPrincipal.Instancia().Seguir1()
                _Cobrar.Close()
        End Select
    End Sub

    Public Function LlenarTabla() As Collection
        Using context As New CiberCafePlusEntities1
            TablaCobrar.Clear()
            Dim sociosQuery = From x In context.VIEW_Socios Select x
            For Each _Consulta In sociosQuery
                TablaCobrar.Add(_Consulta)
            Next
            NotificarCambio("TablaCobrar")
        End Using
        Return TablaCobrar
    End Function
End Class
