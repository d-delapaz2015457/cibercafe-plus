﻿Imports System.ComponentModel

Public Class ModeloVistaModificarCupon
    Implements INotifyPropertyChanged, ICommand
    Private _CSo As ModificarCupon
    Private _TextName As String
    Private _TextClave As String
    Private _TextConfiClave As String
    Private _TextSaldo As Decimal
    Private _Id As Integer

    Private Shared _Instancia As ModeloVistaModificarCupon = Nothing
    Private Shared ReadOnly _Sync As New Object

    Public Shared ReadOnly Property Instancia() As ModeloVistaModificarCupon
        Get
            If _Instancia Is Nothing Then
                SyncLock _Sync
                    If _Instancia Is Nothing Then
                        _Instancia = New ModeloVistaModificarCupon()
                    End If
                End SyncLock
            End If
            Return _Instancia
        End Get
    End Property

    Public Sub Actu(ByRef _instance As ModificarCupon)
        Me._CSo = _instance
    End Sub

    Public Property TextName As String
        Get
            Return _TextName
        End Get
        Set(value As String)
            _TextName = value
            NotificarCambio("TextName")
        End Set
    End Property

    Public Property TextClave As String
        Get
            Return _TextClave
        End Get
        Set(value As String)
            _TextClave = value
            NotificarCambio("TextClave")
        End Set
    End Property

    Public Property TextConfiClave As String
        Get
            Return _TextConfiClave
        End Get
        Set(value As String)
            _TextConfiClave = value
            NotificarCambio("TextConfiClave")
        End Set
    End Property

    Public Property TextSaldo As Decimal
        Get
            Return _TextSaldo
        End Get
        Set(value As Decimal)
            _TextSaldo = value
            NotificarCambio("TextSaldo")
        End Set
    End Property

    '----------- MÉTODOS
    Public Sub NotificarCambio(ByVal Propiedad As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(Propiedad))
    End Sub

    Public Event CanExecuteChanged As EventHandler Implements ICommand.CanExecuteChanged
    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

    Public Function CanExecute(parameter As Object) As Boolean Implements ICommand.CanExecute
        Return True
    End Function

    Public Sub Execute(Parametro As Object) Implements ICommand.Execute
        Select Case Parametro
            Case "Ok"
                Using context As New CiberCafePlusEntities1

                    Dim correct As Integer = 0

                    If IsNumeric(_TextSaldo) Then
                    Else
                        correct = 1
                    End If
                    If _TextSaldo < 100 Then
                        correct = 1
                        MsgBox("El saldo inicial debe ser de 100 en adelante")
                    End If

                    If _TextClave <> _TextConfiClave Then
                        correct = 1
                        MsgBox("Error al verificar la clave")
                    End If

                    If IsNothing(_TextName) Or IsNothing(_TextClave) Or IsNothing(_TextConfiClave) Or IsNothing(_TextSaldo) Then
                        correct = 1
                        MsgBox("No se llenaron todos los campos")
                    End If
                    If correct = 0 Then
                        context.SP_ActuCupones(_TextName, _TextClave, _TextSaldo, True, _Id)
                        _CSo.Close()
                        Dim _Admin As New Cupones
                        _Admin.Show()
                    Else
                    End If

                End Using
            Case "Cancelar"
                MsgBox("Saldrá del registro")
                _CSo.Close()
                Dim _Admin As New Cupones
                _Admin.Show()

            Case "CerrarVentana"
                _CSo.Close()
        End Select
    End Sub

    Public Sub ObtenerId(id As Integer)
        _Id = id
    End Sub
End Class
