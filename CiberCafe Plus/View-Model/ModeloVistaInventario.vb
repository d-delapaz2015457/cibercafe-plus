﻿Imports System.ComponentModel

Public Class ModeloVistaInventario
    Implements INotifyPropertyChanged, ICommand
    Private _Instancia As ModeloVistaInventario
    Private _Inventario As Inventario

    Public Sub New(ByRef _instance As Inventario)
        Me._Inventario = _instance
        Me.Instancia = Me
    End Sub

    '-----TODAS LAS PROPIEDADES ---------
    Public Property Instancia As ModeloVistaInventario
        Get
            Return _Instancia
        End Get
        Set(value As ModeloVistaInventario)
            _Instancia = value
            NotificarCambio("Instancia")
        End Set
    End Property


    '----------- MÉTODOS
    Public Sub NotificarCambio(ByVal Propiedad As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(Propiedad))
    End Sub

    Public Event CanExecuteChanged As EventHandler Implements ICommand.CanExecuteChanged
    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

    Public Function CanExecute(parameter As Object) As Boolean Implements ICommand.CanExecute
        Return True
    End Function

    Public Sub Execute(Parametro As Object) Implements ICommand.Execute
        Select Case Parametro

            Case "Ok"
                _Inventario.Close()

            Case "CerrarVentana"
                _Inventario.Close()
        End Select
    End Sub
End Class
