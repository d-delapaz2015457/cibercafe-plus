﻿Imports System.ComponentModel

Public Class ModeloVistaEspecificacionDinero
    Implements INotifyPropertyChanged, ICommand
    Private _Instancia As ModeloVistaEspecificacionDinero
    Private _EspecificacionDinero As EspecificacionDinero

    Public Sub New(ByRef _instance As EspecificacionDinero)
        Me._EspecificacionDinero = _instance
        Me.Instancia = Me
    End Sub

    '-----TODAS LAS PROPIEDADES ---------
    Public Property Instancia As ModeloVistaEspecificacionDinero
        Get
            Return _Instancia
        End Get
        Set(value As ModeloVistaEspecificacionDinero)
            _Instancia = value
            NotificarCambio("Instancia")
        End Set
    End Property


    '----------- MÉTODOS
    Public Sub NotificarCambio(ByVal Propiedad As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(Propiedad))
    End Sub

    Public Event CanExecuteChanged As EventHandler Implements ICommand.CanExecuteChanged
    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

    Public Function CanExecute(parameter As Object) As Boolean Implements ICommand.CanExecute
        Return True
    End Function

    Public Sub Execute(Parametro As Object) Implements ICommand.Execute
        Select Case Parametro

            Case "Ok"
                _EspecificacionDinero.Close()

            Case "CerrarVentana"
                _EspecificacionDinero.Close()
        End Select
    End Sub

    Private Sub ButtonsChecked(ByVal sender As System.Object,
                               ByVal e As System.Windows.RoutedEventArgs)
        Select Case CType(sender, RadioButton).Name
            Case "RadioButton1"
                'Using context As New Ciber_HacksEntities
                '    Dim adminQuery = From x In context.Monedas Select x
                '    Dim prueba As Integer = 0
                '    For Each moneda In adminQuery
                '        If moneda.name.Equals("Quetzales") And moneda.estado = True Then
                '            prueba = 1
                '            moneda.
                '            Exit For
                '        End If
                '    Next
                '    If prueba = 0 Then
                '        _ValorMoneda = "$"
                '        NotificarCambio("ValorMoneda")
                '    End If
                'End Using
                'Exit Select
            Case "RadioButton2"
                'Do something two
                Exit Select

        End Select
    End Sub

End Class
