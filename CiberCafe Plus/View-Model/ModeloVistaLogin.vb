﻿Imports System.ComponentModel

Public Class ModeloVistaLogin
    Implements INotifyPropertyChanged, ICommand
    Private _Instancia As ModeloVistaLogin
    Private _principal As New MainWindow
    Private _TextName As String
    Private _Contra As String
    Private _Login As Login

    Public Sub New(ByRef _instance As Login)
        Me._Login = _instance
        Me.Instancia = Me
    End Sub

    '-----TODAS LAS PROPIEDADES ---------
    Public Property Instancia As ModeloVistaLogin
        Get
            Return _Instancia
        End Get
        Set(value As ModeloVistaLogin)
            _Instancia = value
            NotificarCambio("Instancia")
        End Set
    End Property

    Public Property TextName As String
        Get
            Return _TextName
        End Get
        Set(value As String)
            _TextName = value
            NotificarCambio("TextName")
        End Set
    End Property

    'Public Property Contra As String
    '    Get
    '        Return _Contra
    '    End Get
    '    Set(value As String)
    '        _Contra = value
    '        NotificarCambio("Contra")
    '    End Set
    'End Property

    '----------- MÉTODOS
    Public Sub NotificarCambio(ByVal Propiedad As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(Propiedad))
    End Sub

    Public Event CanExecuteChanged As EventHandler Implements ICommand.CanExecuteChanged
    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

    Public Function CanExecute(parameter As Object) As Boolean Implements ICommand.CanExecute
        Return True
    End Function

    Public Sub Execute(Parametro As Object) Implements ICommand.Execute
        Select Case Parametro
            Case "BAceptar"
                Using context As New CiberCafePlusEntities1
                    Dim adminQuery = From x In context.Socios Select x
                    Dim _Contra = _Login.Contraseña.Password.ToString()
                    Dim prueba As Integer = 0
                    For Each admin In adminQuery
                        If admin.name.Equals(_TextName) And admin.clave.Equals(_Contra) Then
                            prueba = 1
                            _principal.Show()
                            _Login.Close()
                            MsgBox("             Por favor seleccione un valor de moneda!" & Chr(13) &
                                " " & Chr(13) &
                                "     Vaya a la pestaña: Ventas > Especificar... > Moneda" & Chr(13) &
                                " " & Chr(13) &
                                "               (Si no, se usará por defecto 'Quetzales'!) ")
                            Exit For
                        End If
                    Next
                    If prueba = 0 Then
                        MsgBox("No existe un usuario con esos datos")
                    End If
                End Using

            Case "BCancelar"
                _Login.Close()
                _principal.Close()
        End Select
    End Sub
End Class
