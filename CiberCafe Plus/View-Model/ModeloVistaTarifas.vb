﻿Imports System.ComponentModel

Public Class ModeloVistaTarifas
    Implements INotifyPropertyChanged, ICommand
    Private _Instancia As ModeloVistaTarifas
    Private _Tarifas As Tarifas

    Public Sub New(ByRef _instance As Tarifas)
        Me._Tarifas = _instance
        Me.Instancia = Me
    End Sub

    '-----TODAS LAS PROPIEDADES ---------
    Public Property Instancia As ModeloVistaTarifas
        Get
            Return _Instancia
        End Get
        Set(value As ModeloVistaTarifas)
            _Instancia = value
            NotificarCambio("Instancia")
        End Set
    End Property


    '----------- MÉTODOS
    Public Sub NotificarCambio(ByVal Propiedad As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(Propiedad))
    End Sub

    Public Event CanExecuteChanged As EventHandler Implements ICommand.CanExecuteChanged
    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

    Public Function CanExecute(parameter As Object) As Boolean Implements ICommand.CanExecute
        Return True
    End Function

    Public Sub Execute(Parametro As Object) Implements ICommand.Execute
        Select Case Parametro

            Case "Ok"
                _Tarifas.Close()

            Case "CerrarVentana"
                _Tarifas.Close()
        End Select
    End Sub
End Class
