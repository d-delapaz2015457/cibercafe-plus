﻿Imports System.ComponentModel

Public Class ModeloVistaLoginRetirarEfectivo
    Implements INotifyPropertyChanged, ICommand
    Private _Instancia As ModeloVistaLoginRetirarEfectivo
    Private _LIE As LoginRetirarEfectivo

    Public Sub New(ByRef _instance As LoginRetirarEfectivo)
        Me._LIE = _instance
        Me.Instancia = Me
    End Sub

    '-----TODAS LAS PROPIEDADES ---------
    Public Property Instancia As ModeloVistaLoginRetirarEfectivo
        Get
            Return _Instancia
        End Get
        Set(value As ModeloVistaLoginRetirarEfectivo)
            _Instancia = value
            NotificarCambio("Instancia")
        End Set
    End Property


    '----------- MÉTODOS
    Public Sub NotificarCambio(ByVal Propiedad As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(Propiedad))
    End Sub

    Public Event CanExecuteChanged As EventHandler Implements ICommand.CanExecuteChanged
    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

    Public Function CanExecute(parameter As Object) As Boolean Implements ICommand.CanExecute
        Return True
    End Function

    Public Sub Execute(Parametro As Object) Implements ICommand.Execute
        Select Case Parametro
            Case "Aceptar"
                _LIE.Close()

            Case "CerrarVentana"
                _LIE.Close()


        End Select
    End Sub
End Class
